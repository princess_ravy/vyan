echo "Installing needed packages"
sudo apt install make gcc g++ git curl

echo "Installing nvm"
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh | bash
export NVM_DIR="${XDG_CONFIG_HOME/:-$HOME/.}nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
source ~/.bashrc
echo "Installing latest node v10"
nvm install 10 --no-progress
nvm use 10

echo "Installing dependencies"
npm i --quiet
npm uninstall discord.js klasa --quiet
npm i dirigeants/klasa#4d9b9ba9c216e5b18a4d79e45ba3c2dfe9b89ce9 discordjs/discord.js#840d22f8920f7648f97109d3df6f8a9b1da9f20f --quiet

echo "Installing pm2"
npm i pm2 -g

