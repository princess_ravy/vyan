const { Task } = require('klasa');

module.exports = class extends Task {

	async run({ user }) {
		const reason = 'Automatic unban (temporary ban)';
		const syncedGuilds = this.client.settings.get('syncedGuilds');
		if (!syncedGuilds.length) return;
		this.client.users.fetch(user.id);
		for (let i = 0; i < syncedGuilds.length; i++) {
			if (this.client.guilds.get(syncedGuilds[i]).me.hasPermission('BAN_MEMBERS')) {
				this.client.guilds.get(syncedGuilds[i]).members.unban(user.id, reason).catch(() => null);
			}
		}
		this.client.settings.update('syncBannedUsers', user.id, { action: 'remove' });
	}

};
