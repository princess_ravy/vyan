const { Task } = require('klasa');

module.exports = class extends Task {

	async run({ channel }) {
		const _channel = this.client.channels.get(channel);
		if (!_channel) return;
		return _channel.delete().catch(() => null);
	}

};
