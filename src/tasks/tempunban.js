const { Task } = require('klasa');

module.exports = class extends Task {

	async run({ guild, user }) {
		const _guild = this.client.guilds.get(guild);
		return _guild.members.unban(user, 'Automatic unban (temporarily banned).').catch(() => null);
	}

};
