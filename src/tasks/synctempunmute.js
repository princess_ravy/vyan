const { Task } = require('klasa');

module.exports = class extends Task {

	async run({ user }) {
		const reason = 'Automatic unmute (temporary mute)';
		for (const guild of this.client.settings.syncedGuilds) {
			const _guild = this.client.guilds.get(guild);
			await _guild.members.fetch(user).catch(() => null);
			const _member = _guild.members.get(user).catch(() => null);
			if (!_member) continue;
			_member.roles.remove(_guild.settings.muteRole, reason).catch(() => null);
		}
	}

};
