const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['setPrefix'],
			cooldown: 5,
			description: 'Change the command prefix the bot uses in your server.',
			runIn: ['text'],
			usage: '[reset|prefix:str{1,10}]'
		});
	}

	async run(msg, [prefix]) {
		if (!prefix) return msg.responder.info(`Prefix for this guild is set to \`${msg.guild.settings.prefix}\`.`);
		if (prefix === 'reset') return this.reset(msg);
		if (!prefix) return msg.responder.error('Prefix too long.');
		if (msg.guild.settings.prefix === prefix) return msg.responder.error(msg.language.get('CONFIGURATION_EQUALS'));
		await msg.guild.settings.update('prefix', prefix);
		return msg.responder.success(`The prefix for this guild has been set to \`${prefix}\`.`);
	}

	async reset(msg) {
		await msg.guild.settings.update('prefix', this.client.options.prefix);
		return msg.responder.success(`Switched back the guild's prefix back to \`${this.client.options.prefix}\`.`);
	}

};
