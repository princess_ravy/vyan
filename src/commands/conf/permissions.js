const { Command } = require('klasa');
const nodeValidation = /[a-z]?(\.[a-z*])?/;

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'Configure permissions for this guild.',
			usage: '<allow|deny|reset> <everyone|user:member|channel:channel|role:role|role:rolename> [node:str]',
			usageDelim: ' ',
			guarded: true,
			aliases: ['perms'],
			quotedStringSupport: true
		});
	}

	async run(msg, [identifier, entity, node]) {
		node = node.replace('.*', '');
		const [category, command] = node.split('.');
		if (!['conf', 'fun', 'info', 'mod', 'nsfw', 'util', 'xp'].includes(category)) return msg.responder.error('Invalid category.');
		if (command) {
			const parsedCommand = this.client.commands.get(command);
			if (!parsedCommand) return msg.responder.error('Invalid Command.');
			if (!(parsedCommand.category === category)) return msg.responder.error('Invalid permission node.');
			node = `${category}.${parsedCommand.name.replace('-', '')}`;
		}
		if (entity === 'everyone') entity = msg.guild.roles.get(msg.guild.id);
		if (identifier === 'reset') await this.resetPermissions(entity, msg);
		if (identifier === 'allow') await this.allowPermissions(entity, node, msg);
		if (identifier === 'deny') await this.denyPermissions(entity, node, msg);
		return msg.reactor.success();
	}

	async allowPermissions(entity, node, msg) {
		if (!node || !nodeValidation.test(node)) return msg.responder.error('Please specify a valid permission node.');
		await entity.customPermissions.update('denied', node, { action: 'remove' });
		return entity.customPermissions.update('allowed', node, { action: 'add' });
	}

	async denyPermissions(entity, node, msg) {
		if (!node || !nodeValidation.test(node)) return msg.responder.error('Please specify a valid permission node.');
		await entity.customPermissions.update('allowed', node, { action: 'remove' });
		return entity.customPermissions.update('denied', node, { action: 'add' });
	}

	async resetPermissions(entity) {
		return entity.customPermissions.reset(['allowed', 'denied']);
	}

};
