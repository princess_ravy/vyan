const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['log', 'setLog', 'setLogChannel', 'modlog'],
			runIn: ['text'],
			description: 'Toggle logging.',
			usage: '<channel:channelname|disable:default> [members|moderation|messages] [...]'
		});
	}

	async run(msg, [logChannel, ...modules]) {
		if (logChannel === 'disable') {
			await msg.guild.log.reset('channel');
			await msg.guild.log.reset('webhook');
		} else {
			await msg.guild.log.update('channel', logChannel);
			const logWebhook = await logChannel.createWebhook(`${this.client.user.username} Logs`,
				{
					avatar: this.client.user.avatarURL({ format: 'png', size: 2048 }),
					reason: `Setting up logging for ${this.client.user.username}`
				}).catch(() => null);
			if (!logWebhook) return msg.responder.error(`Failed creating Webhook because of missing permissions in ${logChannel}`);
			await msg.guild.log.update('webhook', logWebhook.id);
			for (const module of modules) {
				await msg.guild.log.update(`log.${module}`, true);
			}
		}
		return msg.reactor.success();
	}

};
