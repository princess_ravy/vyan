const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['lr', 'lvrole', 'levelrole', 'lvlrole', 'lvroles', 'lrs', 'lvlroles'],
			runIn: ['text'],
			description: 'Sets up roles gained by experience.',
			usage: '<add|remove|list:default> [level:int] [rolename:rolename]',
			usageDelim: ' ',
			subcommands: true
		});
	}

	async add(msg, [level, role]) {
		if (!level || !role) return msg.responder.error('Missing args.');
		const { id } = role;
		for (const levelrole of msg.guild.settings.levelroles) {
			if (levelrole.id === id) return msg.responder.error(`That role already is assigned at level ${levelrole.level}.`);
		}
		await msg.guild.settings.update('levelroles', { id, level }, { action: 'add' });
		return msg.responder.success(`**${role.name}** will be automatically assigned when level **${level}** is hit.`);
	}

	async remove(msg, [role]) {
		if (!level || !role) return msg.responder.error('Missing args.');
		const { id } = role;
		for (const levelrole of msg.guild.settings.levelroles) {
			if (levelrole.id === id) {
				await msg.guild.settings.update('levelroles', levelrole, { action: 'remove' });
				return msg.responder.success(`**${role.name}** will no longer be automatically assigned.`);
			}
		}
		return msg.responder.error("That role isn't automatically assigned");
	}

	async list(msg) {
		if (!msg.guild.settings.levelroles.length) return msg.send("There haven't been any levelroles set up yet.");
		const roleNames = [];
		for (const levelrole of msg.guild.settings.levelroles) {
			if (msg.guild.roles.has(levelrole.id)) {
				const role = msg.guild.roles.get(levelrole.id);
				roleNames.push(`**Lv. ${levelrole.level}**: ${role.name}`);
			}
		}
		return msg.send({ embed: {
			title: `LevelRoles on ${msg.guild.name}`,
			description: roleNames.sort().join('\n') || 'none'
		} });
	}

};
