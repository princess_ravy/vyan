const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['joinrole', 'ar'],
			cooldown: 5,
			description: 'Configure the AutoRole for this guild.',
			requiredPermissions: ['ADD_REACTIONS', 'SEND_MESSAGES'],
			runIn: ['text'],
			usage: '<set|remove> <join|bot> [role:rolename]',
			usageDelim: ' ',
			subcommands: true,
			quotedStringSupport: true
		});
	}

	async set(msg, [selector, role]) {
		if (!role) {
			return msg.responder.error('No role specified.');
		}
		if (!(msg.guild.members.get(this.client.user.id).roles.highest.rawPosition > role.rawPosition)) {
			return msg.responder.error("I can't assign that role.");
		}
		if (selector === 'join') {
			await msg.guild.settings.update('autoRole', role, msg.guild, { action: 'add' });
			return msg.responder.success(`AutoRole has been updated to **${role.name}**.`);
		} else if (selector === 'bot') {
			await msg.guild.settings.update('botRole', role, msg.guild, { action: 'add' });
			return msg.responder.success(`BotRole has been updated to **${role.name}**.`);
		}
		return msg.responder.error('Unknown Error');
	}

	async remove(msg, [selector]) {
		if (selector === 'join') {
			await msg.guild.settings.reset('autoRole');
			return msg.reactor.success();
		} else if (selector === 'bot') {
			await msg.guild.settings.reset('botRole');
			return msg.reactor.success();
		}
		return msg.responder.error('Unknown Error');
	}

};
