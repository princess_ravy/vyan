const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			permissionLevel: 10,
			description: 'Changes some important stuff.',
			usage: '<avatar|username|nickname> <URL:Hyperlink|Name:String> [...]',
			subcommands: true,
			usageDelim: ' '
		});
	}

	async avatar(msg, [URL]) {
		this.client.user.setAvatar(URL);
		return msg.reactor.success();
	}

	async username(msg, [...name]) {
		this.client.user.setUsername(name.join(' '));
		return msg.reactor.success();
	}

	async nickname(msg, [...name]) {
		if (!msg.guild) return msg.reactor.error();
		msg.guild.me.setNickname(name.join(' '));
		return msg.reactor.success();
	}

};
