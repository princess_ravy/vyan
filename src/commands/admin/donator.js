const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			permissionLevel: 10,
			guarded: true,
			description: 'Add or remove donators from the synced list.',
			usage: '<add|remove> <user:user>',
			usageDelim: ' '
		});
	}

	async run(msg, [action, user]) {
		await this.client.settings.update('donators', user.id, { action });
		return msg.reactor.success();
	}

};
