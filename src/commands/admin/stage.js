const { Command } = require('klasa');
const git = require('simple-git/promise');
const remote = `https://${process.env.GITLAB_USER}:${process.env.GITLAB_PASS}@${process.env.GITLAB_REPO}`;

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			permissionLevel: 10,
			guarded: true,
			description: 'Apply an update to the repo.',
			usage: '[commit_message:str]'
		});
	}

	async run(msg, [message]) {
		if (!message) message = await require('centra')('http://whatthecommit.com/index.txt').send().then(r => r.res.text());
		await git().commit(message, { '--all': null });
		await git().push(remote, 'master');
		return msg.responder.success(`Staging complete.`);
	}

};
