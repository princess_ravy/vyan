const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			permissionLevel: 10,
			guarded: true,
			aliases: ['shutdown'],
			description: 'Logs out, terminates the connection to Discord, and destroys the client.'
		});
	}

	async run(msg) {
		await msg.responder.success('Shutting down');
		this.client.destroy();
		process.exit();
	}

};
