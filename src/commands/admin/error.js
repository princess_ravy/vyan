const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			permissionLevel: 10,
			aliases: ['err'],
			description: 'Get an error by code.',
			usage: '<code:str>'
		});
	}

	async run(msg, [code]) {
		if (code === 'reset') {
			await this.client.settings.reset('errors');
			return msg.reactor.success();
		}
		const error = this.client.settings.errors.find(err => err.code === code);
		if (!error) return msg.responder.error("That error doesn't exist.");
		return msg.responder.info(
			`${error.command} ${JSON.stringify(error.args.map(arg => arg && arg.length > 100 ? 'too long' : arg))}${JSON.stringify(error.params)}`,
			`\`\`\`js\n${JSON.stringify(error.error)}\n\`\`\``,
			error.code
		).catch(() => msg.responder.error('Error too long.'));
	}

};
