const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			permissionLevel: 1,
			guarded: true,
			usage: '<user:user> <content:string> [...]',
			usageDelim: ' ',
			aliases: ['reply'],
			description: 'Sends DMs. Wow.'
		});
	}

	async run(msg, [user, ...content]) {
		this.client.users.get(user.id).send(content.join(' '));
		return msg.reactor.success();
	}

};
