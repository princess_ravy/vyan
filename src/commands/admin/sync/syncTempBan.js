const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			permissionLevel: 3,
			description: 'Ban a user on all synced guilds temporarily.',
			usage: '<user:user> <time:time> [reason:string] [...]',
			usageDelim: ' ',
			aliases: ['stb'],
			requiredPermissions: ['BAN_MEMBERS']
		});
	}

	async run(msg, [user, time, ...reason]) {
		if (this.client.settings.syncedAdmins.includes(user.id)) return msg.responder.error('You cannot ban a syncAdmin.');
		if (!reason || !reason.length) {
			reason = 'no reason provided';
		} else {
			reason = reason.join(' ');
		}
		const syncedGuilds = this.client.settings.get('syncedGuilds');
		if (!syncedGuilds.length) {
			return msg.responder.error('There are no synced guilds.');
		}
		await this.client.users.fetch(user.id);
		for (let i = 0; i < syncedGuilds.length; i++) {
			this.client.guilds.get(syncedGuilds[i]).members.ban(user.id, { reason: `${msg.author.tag} | ${reason} (unban at ${time})` });
		}

		await this.client.settings.update('syncBannedUsers', user.id, { action: 'add' });

		await this.client.schedule.create('synctempunban', time, {
			data: {
				user: user.id
			}
		});

		await msg.guild.logger.syncTempBan({ user, reason, time, moderator: msg.author });
		return msg.reactor.ban();

		// msg.channel.send(`**${this.client.users.get(user.id).tag}** has been banned from:\n\n❯ ${bannedOn.length > 0 ? bannedOn.join('\n❯ ') : 'nowhere'}`);
	}

};
