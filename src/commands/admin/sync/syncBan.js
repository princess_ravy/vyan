const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			permissionLevel: 3,
			description: 'Ban a user on all synced guilds.',
			usage: '<user:user> [reason:string] [...]',
			usageDelim: ' ',
			aliases: ['sb'],
			requiredPermissions: ['BAN_MEMBERS']
		});
	}

	async run(msg, [user, ...reason]) {
		if (this.client.settings.syncedAdmins.includes(user.id)) return msg.responder.error("Can't ban a syncAdmin.");
		if (!reason || !reason.length) {
			reason = 'no reason provided';
		} else {
			reason = reason.join(' ');
		}
		const syncedGuilds = this.client.settings.get('syncedGuilds');
		if (!syncedGuilds.length) {
			return msg.responder.error('There are no synced guilds.');
		}
		await this.client.users.fetch(user.id);
		const bannedOn = [];
		const missingPermissions = [];
		for (let i = 0; i < syncedGuilds.length; i++) {
			if (!this.client.guilds.get(syncedGuilds[i]).me.hasPermission('BAN_MEMBERS' || !this.client.guilds.get(syncedGuilds[i]).members.get(user.id).bannable)) {
				missingPermissions.push(this.client.guilds.get(syncedGuilds[i]).name);
				// msg.channel.send(`I'm missing ban permission on ${this.client.guilds.get(syncedGuilds[i]).name}.`)
			} else if (this.client.guilds.get(syncedGuilds[i]).members.get(user.id) === undefined || this.client.guilds.get(syncedGuilds[i]).members.get(user.id).bannable) {
				await this.client.guilds.get(syncedGuilds[i]).members.ban(user.id, { reason: `${msg.author.tag} | ${reason}` });
				bannedOn.push(this.client.guilds.get(syncedGuilds[i]).name);
			}
		}

		await this.client.settings.update('syncBannedUsers', user.id, { action: 'add' });

		if (missingPermissions.length) {
			await msg.channel.send({ embed: {
				title: `Missing ban permissions on`,
				description: `❯ ${missingPermissions.join('\n❯ ')}`,
				color: 16007990
			} });
		}

		await msg.guild.logger.syncBan({ user, reason, moderator: msg.author });
		if (bannedOn.length) return msg.reactor.ban();
		return msg.reactor.error();
		// msg.channel.send(`**${this.client.users.get(user.id).tag}** has been banned from:\n\n❯ ${bannedOn.length > 0 ? bannedOn.join('\n❯ ') : 'nowhere'}`);
	}

};
