/* eslint-disable id-length */
const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			permissionLevel: 10,
			description: 'Syncs promoting/demoting.',
			usage: '<add|remove> <User:user> <admin|mod|sup>',
			subcommands: true,
			usageDelim: ' ',
			aliases: ['srank']
		});
	}

	async add(msg, [user, rank]) {
		let identifier;
		if (rank === 'admin') identifier = 'Admins';
		if (rank === 'mod') identifier = 'Mods';
		if (rank === 'sup') identifier = 'Sups';
		if (!identifier) return msg.reactor.error();
		await this.client.settings.update(`synced${identifier}`, user);
		return msg.reactor.success();
	}

};
