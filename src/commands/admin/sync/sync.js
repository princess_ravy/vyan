const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			permissionLevel: 10,
			guarded: true,
			description: 'Adds a guild to the sync',
			usage: '<add|rem|prune|list:default> [Guild:Guild]',
			subcommands: true,
			usageDelim: ' '
		});
	}

	async add(msg, [guild = msg.guild]) {
		if (!guild) {
			return msg.responder.error('You need to include a guild to sync.');
		}
		if (!this.client.settings.syncedGuilds.includes(guild.id)) {
			await this.client.settings.update('syncedGuilds', guild.id);
			return msg.responder.success(`Now syncing guild **${guild.name}**.`);
		} else {
			return msg.responder.error('That guild is already synced.');
		}
	}

	async rem(msg, [guild]) {
		if (!guild) {
			return msg.responder.error('You need to include a guild to unsync.');
		}
		if (this.client.settings.syncedGuilds.includes(guild.id)) {
			await this.client.settings.update('syncedGuilds', guild.id);
			return msg.responder.success(`Stopped syncing guild **${guild.name}**.`);
		} else {
			return msg.responder.error('That guild is not synced.');
		}
	}

	async prune(msg) {
		let amount = 0;
		for (const id of this.client.settings.syncedGuilds) {
			if (!this.client.guilds.has(id)) {
				this.client.settings.update('syncedGuilds', id, { action: 'remove' });
				amount++;
			}
		}
		return msg.responder.success(amount > 0 ? `Pruned ${amount} guilds from syncing.` : 'All guilds still available.');
	}

	async list(msg) {
		const guildNames = await this.getList();
		msg.channel.send({ embed: {
			title: 'Synced Guilds',
			description: guildNames
		} });
	}

	getList() {
		const guildNames = [];
		for (const guild of this.client.settings.syncedGuilds) {
			guildNames.push(this.client.guilds.get(guild).name);
		}
		return guildNames.join('\n');
	}

	getListExperimental() {
		return this.client.guilds.filter(g => this.client.settings.syncedGuilds.includes(g.id)).map(g => g.name).join('\n');
	}

};
