const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			permissionLevel: 10,
			description: 'Get message.language.'
		});
	}

	async run(msg) {
		const res = msg.prompt('Wha', '<y|n|idk|p|pn>', { time: 1234567 });
		msg.send(res);
	}

};
