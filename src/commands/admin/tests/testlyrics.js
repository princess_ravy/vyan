const { Command } = require('klasa');
const req = require('centra');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			permissionLevel: 10,
			description: 'Get lyrics from Gaysoft.',
			usage: '[query:str]',
			usageDelim: ' '
		});
	}

	async run(msg, [query]) {
		const res = await req(`https://api.ksoft.si/lyrics/search?q=${encodeURIComponent(query)}`)
			.header('Authorization', `Bearer ${process.env.API_KSOFT}`)
			.send()
			.then(r => r.json());

		for (const item in res.data) {
			res.data[item].lyrics = res.data[item].lyrics.split('\n').slice(0, 4).join('\n');
		}

		return msg.channel.send(`\`\`\`js\n${JSON.stringify(res, null, 2)}\n\`\`\``);
	}

};
