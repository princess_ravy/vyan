const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			permissionLevel: 10,
			description: 'Get message.language.',
			usage: '<no_reason:str> [reason:str] [...]',
			usageDelim: ' '
		});
	}

	async run(msg) {
		return msg.responder.info(JSON.stringify(msg.params, null, 2));
	}

};
