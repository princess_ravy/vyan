const { Command, Type } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			permissionLevel: 10,
			description: 'Get new usage.',
			usage: '<members:...member>',
			usageDelim: ' '
		});
	}

	async run(msg, [members]) {
		return msg.sendCode('js', `${new Type(members)} ${JSON.stringify(members, null, 4)}`);
	}

};
