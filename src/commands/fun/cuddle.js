const { Command } = require('klasa');
const get = require('centra');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'Cuddle with someone! Please!',
			usage: '<somebody:user>',
			usageDelim: ' '
		});
	}

	async run(msg, [user]) {
		let self;
		if (user.id === msg.author.id) self = true;
		const { url } = await get('https://nekos.life/api/v2/img/cuddle').send().then(r => r.json());
		msg.channel.send({
			embed: {
				description: self ? `${user} cuddled themselves. How is that even possible.` : `${msg.author} cuddled ${user}. How cute. 💕`,
				image: {
					url
				}
			}
		});
	}

};
