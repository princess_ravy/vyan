/* eslint-disable no-unused-vars, camelcase */
const get = require('centra');
const { Command } = require('klasa');
const singleQuotes = /&#(?:0*)?(39);/g;
const doubleQuotes = /&quot;/g;

const baseURL = 'https://opentdb.com/api.php?amount=1';

const categories = new Map()
	.set('General', 9)
	.set('Books', 10)
	.set('Film', 11)
	.set('Music', 12)
	.set('Culture', 13)
	.set('Television', 14)
	.set('Video Games', 15)
	.set('Board Games', 16)
	.set('Nature', 17)
	.set('Computers', 18)
	.set('Mathematics', 19)
	.set('Mythology', 20)
	.set('Sports', 21)
	.set('Geography', 22)
	.set('History', 23)
	.set('Politics', 24)
	.set('Art', 25)
	.set('Celebrities', 26)
	.set('Animals', 27)
	.set('Vehicles', 28)
	.set('Comics', 29)
	.set('Gadgets', 30)
	.set('Weeb', 31)
	.set('Cartoon', 32);

const difficultyPoints = new Map()
	.set('easy', 10)
	.set('medium', 25)
	.set('hard', 50);

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'Trivia game. The classic.',
			cooldown: 3
		});
	}

	async run(msg, [cat]) {
		const { results } = await get(baseURL).send().then(r => r.json());
		const [result] = results;
		const { question, difficulty, correct_answer, incorrect_answers, category } = result;
		const possible_answers = incorrect_answers.concat([correct_answer]).sort().map(a => a
			.toLowerCase()
			.replace(singleQuotes, "'")
			.replace(doubleQuotes, '"'));
		let i = 0;
		await msg.responder.info(`Trivia / ${category}`, [`**${question
			.replace(singleQuotes, "'")
			.replace(doubleQuotes, '"')}**`]
			.concat(possible_answers.map(a => { i++; return `\`${i}\` ${a}`; }))
			.join('\n'), `difficulty: ${difficulty}`);
		const filter = m => possible_answers.includes(m.content.toLowerCase()) || ['1', '2', '3', '4'].slice(0, possible_answers.length).includes(m.content);
		const collector = msg.channel.createMessageCollector(filter, { time: 20000 });
		const repliedUsers = [];
		let correct = false;
		collector.on('collect', m => {
			if (repliedUsers.includes(m.author.id)) return m.responder.error('You already replied to this question.');
			repliedUsers.push(m.author.id);
			if (['1', '2', '3', '4'].slice(0, possible_answers.length).includes(m.content)) m.content = possible_answers[parseInt(m.content) - 1];
			if (m.content.toLowerCase() === correct_answer.toLowerCase()) {
				correct = true;
				collector.stop();
				m.author.settings.update('trivia', m.author.settings.trivia + difficultyPoints.get(difficulty));
				return m.responder.success(`${m.author} got the right answer with: **${correct_answer}**.`);
			}
			return m.responder.error(`Sorry, ${m.author}, **${m.content}** is not the correct answer.`);
		});

		collector.on('end', () => {
			if (!correct) msg.responder.error(`Seems like noone got it. The correct answer was **${correct_answer.toLowerCase().replace(singleQuotes, "'").replace(doubleQuotes, '"')}**.`);
		});
	}

};
