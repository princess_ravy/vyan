const get = require('centra');
const { Command } = require('klasa');
const { User } = require('discord.js');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'A bit of magik.',
			usage: '[image:url|user:user]',
			cooldown: 5
		});
	}

	async run(msg, [link = msg.author]) {
		if (link instanceof User) link = link.displayAvatarURL({ format: 'png', size: 2048 });
		const { message: img } = await get('https://nekobot.xyz/api/imagegen')
			.query({
				type: 'magik',
				image: link
			})
			.send()
			.then(r => r.json());
		return msg.responder.image(img);
	}

};
