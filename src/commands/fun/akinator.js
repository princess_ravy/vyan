const { Command } = require('klasa');
const { MessageEmbed } = require('discord.js');
const aki = require('aki-api');
const region = 'en';
const answerString = '<y|n|idk|p|pn>';
const answerStringMap = new Map()
	.set('y', 0)
	.set('n', 1)
	.set('idk', 2)
	.set('p', 3)
	.set('pn', 4);

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['aki'],
			description: 'Play a game with the one and only Akinator.'
		});
		this.donatorOnly = true;
	}

	async run(msg) {
		// Starts Aki
		const data = await aki.start(region);
		let step = 0;
		// Collect first reply
		const [firstUnparsedReply] = await msg.createPrompt(`**Question 1:** ${data.question}`, answerString);
		if (!answerStringMap.has(firstUnparsedReply)) return msg.reactor.error();
		const firstParsedReply = answerStringMap.get(firstUnparsedReply);

		let nextInfo = await aki.step(region, data.session, data.signature, firstParsedReply, step);
		while (nextInfo.progress < 98) {
			const [unparsedReply] = await msg.createPrompt(`**Question ${step + 2}:** ${nextInfo.nextQuestion}`, answerString);
			if (!answerStringMap.has(unparsedReply)) return msg.reactor.error();
			const parsedReply = answerStringMap.get(unparsedReply);
			step++;
			nextInfo = await aki.step(region, data.session, data.signature, parsedReply, step);
		}
		const win = await aki.win(region, data.session, data.signature, step + 1);
		const firstGuess = win.answers[0];
		const embed = new MessageEmbed()
			.setTitle(firstGuess.name)
			.setDescription(firstGuess.description)
			.setFooter(`id ${firstGuess.id} | probability: ${Math.round(firstGuess.proba * 10000) / 100}%`)
			.setImage(firstGuess.absolute_picture_path);
		return msg.send({ embed });
	}

};
