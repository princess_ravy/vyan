const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'Gets your or someone else\'s trivia score.',
			usage: '[user:user]'
		});
	}

	async run(msg, [user]) {
		if (!user) user = msg.author;
		return msg.responder.info(`${user === msg.author ? 'You have' : `${user} has`} **${user.settings.trivia}** trivia points.`);
	}

};
