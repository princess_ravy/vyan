const get = require('centra');
const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'Mmmmmhhh.',
			usage: '<user:username>',
			cooldown: 5
		});
	}

	async run(msg, [user]) {
		const { message: img } = await get(`https://nekobot.xyz/api/imagegen`)
			.query({
				type: 'deepfry',
				image: user.displayAvatarURL({ format: 'png', size: 2048 })
			})
			.send()
			.then(r => r.json());
		return msg.responder.image(img);
	}

};
