const { Command } = require('klasa');
const get = require('centra');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'Kiss someone! Show your love!',
			usage: '<somebody:user>',
			usageDelim: ' '
		});
	}

	async run(msg, [user]) {
		let self;
		if (user.id === msg.author.id) self = true;
		const { url } = await get('https://nekos.life/api/v2/img/kiss').send().then(r => r.json());
		msg.channel.send({
			embed: {
				description: self ? `${user} kissed themselves. I have no idea how and why.` : `${msg.author} gave ${user} a kiss. How cute. 💕`,
				image: {
					url
				}
			}
		});
	}

};
