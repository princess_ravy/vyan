const { Command } = require('klasa');
const get = require('centra');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['randomcat', 'meow'],
			requiredPermissions: ['EMBED_LINKS', 'ATTACH_FILES'],
			description: 'Grabs a random cat image from random.cat.'
		});
	}

	async run(msg) {
		const { file } = await get('http://aws.random.cat/meow').send().then(r => r.json());
		return msg.channel.sendFile(file, `cat.${file.slice(file.lastIndexOf('.'), file.length)}`);
	}

};
