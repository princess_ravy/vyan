/* eslint-disable id-length */
const { Command } = require('klasa');
const options = {
	max: 1,
	time: 30000,
	errors: ['time']
};

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'Starts a quiz with interesting questions.'
		});
	}

	async run(msg) {
		const quiz = this.client.settings.get('quiz');
		if (quiz.length < 1) return msg.responder.error("There haven't been any questions added yet.");
		const item = quiz[Math.floor(Math.random() * quiz.length)];
		await msg.channel.send(item.q);
		try {
			const collected = await msg.channel.awaitMessages(answer => item.a.includes(answer.content.toLowerCase()), options);
			const winnerMessage = collected.first();
			await msg.author.settings.update('quiz', msg.author.settings.quiz + 1);
			return msg.responder.success(`We have a winner! *${winnerMessage.author.username}* had a right answer with \`${winnerMessage.content}\`!`);
		} catch (_) {
			return msg.responder.error('Seems no one got it! Oh well.');
		}
	}

};
