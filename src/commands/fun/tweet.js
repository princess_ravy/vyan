const get = require('centra');
const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'Tweet as someone else.',
			usage: '<twitter_handle:str{1,30}> <message:str{1,140}> [...]',
			usageDelim: ' ',
			cooldown: 5
		});
	}

	async run(msg, [handle, ...message]) {
		const { message: img } = await get('https://nekobot.xyz/api/imagegen')
			.query({
				type: 'tweet',
				username: handle,
				text: message.join(' ')
			})
			.send()
			.then(r => r.json());
		return msg.responder.image(img);
	}

};
