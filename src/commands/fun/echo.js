const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			runIn: ['text'],
			description: 'Send a message to a channel through the bot.',
			usage: '[channel:channel] <message:string> [...]',
			aliases: ['say'],
			usageDelim: ' '
		});
		this.donatorOnly = true;
	}

	async run(msg, [channel = msg.channel, ...message]) {
		if (msg.flags.delete) msg.delete().catch(() => null);
		if (channel.postable === false && channel !== msg.channel) return msg.responder.error('The selected channel is not postable.');
		return channel.send(message.join(' '));
	}

};
