const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'Propose to marry someone.',
			usage: '<your_beloved_one:user>',
			aliases: ['marry'],
			cooldown: 43200
		});
	}

	async run(msg, [user]) {
		if (user.id === this.client.user.id) return msg.responder.error('Ummmm, I\'m sorry, but that doesn\'t work :c');
		if (user.bot) return msg.responder.error('Robotic love, huh? Sadly that\'s not possible.');
		if (user.id === msg.author.id) return msg.responder.error('I love myself, too. But you sadly can\'t marry yourself.');
		if (user.settings.proposals.includes(msg.author.id)) return msg.responder.error('You already proposed to that person.');

		await user.settings.update('proposals', msg.author);

		return msg.channel.send({ embed: {
			title: `${msg.author.username} just proposed to ${user.username} 💕`,
			description: `${user}, type \`${msg.guild.settings.prefix}proposal accept|decline @${msg.author.tag}\` to get married or turn down the proposal.`
		} });
	}

};
