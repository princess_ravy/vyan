/* eslint-disable new-cap */
const { Command } = require('klasa');
const RAVEN = '102102717165506560', ASKARA = '305407776199540736';

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['shipping', 'love', 'lovetest'],
			requiredPermissions: ['EMBED_LINKS'],
			description: 'Returns the percentage two users should be together.',
			usage: '<first_name:user> <second_name:user>',
			usageDelim: ' '
		});
	}

	async run(msg, [name1, name2]) {
		const ship1 = (BigInt(name1.id) >> 22n) % 51n;
		const ship2 = (BigInt(name2.id) >> 22n) % 51n;
		let shipPercentage = ship1 + ship2;
		if ((name1.id === RAVEN && name2.id === ASKARA) || (name2.id === RAVEN && name1.id === ASKARA)) shipPercentage = 100;
		let ship = '';
		if (shipPercentage <= 30) ship = 'Ouch! 💔';
		else if (shipPercentage <= 60) ship = 'Mhhhm! ❣';
		else if (shipPercentage <= 80) ship = 'Ooooh! ❤';
		else if (shipPercentage <= 99) ship = 'A sweet couple! 💗';
		else ship = 'Destined for each other! 💞';
		return msg.channel.send({
			embed: {
				fields: [
					{
						name: `${ship}`,
						value: `💒 ${name1}\n💒 ${name2}\nThere's a **${shipPercentage}%** Chance of you two coming together.`
					}
				],
				color: 16164043
			}
		});
	}

};
