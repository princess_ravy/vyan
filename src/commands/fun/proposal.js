const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'Propose to marry someone.',
			aliases: ['proposals'],
			usage: '[accept|decline] [your_beloved_one:user]',
			subcommands: true,
			usageDelim: ' '
		});
	}

	async run(msg) {
		const proposals = [];
		for (const user of msg.author.settings.proposals) {
			proposals.push(this.client.users.get(user).tag);
		}
		return msg.channel.send({ embed: {
			description: proposals.join('\n') || 'none'
		} });
	}

	async accept(msg, [user]) {
		if (!user) return msg.responder.error('You need to define whose proposal to accept o.O');
		if (!msg.author.settings.proposals.includes(user.id)) return msg.responder.error("That person didn't propose to you o.O");
		if (msg.author.settings.marriedTo) return msg.responder.error("You're already married!");
		await msg.author.settings.update('proposals', user, { action: 'remove' });
		await msg.author.settings.update('marriedTo', user, { action: 'add' });
		await user.settings.update('marriedTo', msg.author, { action: 'add' });
		return msg.channel.send({ embed: {
			title: `${user.username} and ${msg.author.username} are now happily married! 💕`,
			description: 'Best of luck and lots of love to you two!',
			color: 16164043
		} });
	}

	async decline(msg, [user]) {
		if (!user) return msg.responder.error('You need to define whose proposal to turn down o.O');
		if (!msg.author.settings.proposals.includes(user.id)) return msg.responder.error("That person didn't propose to you o.O");
		await msg.author.settings.update('proposals', user, { action: 'remove' });
		return msg.channel.send({ embed: {
			title: `${msg.author.username} turned down the proposal of ${user.username}`,
			description: "Aww :c Don't worry, there'll always be a next one!"
		} });
	}

};
