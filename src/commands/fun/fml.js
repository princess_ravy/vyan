/* eslint-disable id-length */
const { Command } = require('klasa');
const { MessageEmbed } = require('discord.js');
const request = require('request');
const cheerio = require('cheerio');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, { description: 'Gets a random FML story.', enabled: true });
	}

	async run(msg) {
		request(
			{
				uri: 'http://www.fmylife.com/random'
			},
			(error, response, body) => {
				if (error) return msg.channel.send(`Error: ${error}`);
				var $ = cheerio.load(body);
				const article = $('.article-link').first().text();
				const updoot = $('.vote-up-group .vote-count').first().text();
				const downdoot = $('.vote-down-group .vote-count').first().text();

				const embed = new MessageEmbed()
					.setTitle(`Requested by ${msg.author.tag}`)
					.setAuthor('FML Stories')
					.setColor(msg.member.displayColor)
					.setTimestamp()
					.setDescription(`_${article}\n\n_`)
					.addField('I agree, your life sucks', updoot, true)
					.addField('You deserved it', downdoot, true);

				return msg.sendMessage({ embed });
			}
		);
	}

};
