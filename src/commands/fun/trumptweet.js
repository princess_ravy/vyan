const get = require('centra');
const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'Tweet as the POTUS. Very nice.',
			usage: '<text:str{1,140}>',
			cooldown: 5
		});
	}

	async run(msg, [text]) {
		const { message: img } = await get('https://nekobot.xyz/api/imagegen')
			.query({
				type: 'trumptweet',
				text
			})
			.send()
			.then(r => r.json());
		return msg.responder.image(img);
	}

};
