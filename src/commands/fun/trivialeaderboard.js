const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'Gets the best trivia players. Wow.',
			aliases: ['trivlb', 'trivialb']
		});
	}

	async run(msg) {
		let i = 0;
		const users = await this.client.users
			.filter(u => u.settings.trivia)
			.sort((a, b) => b.settings.trivia - a.settings.trivia)
			.first(10)
			.map(u => { i++; return `\`${i === 10 ? '' : '0'}${i}\` ${u.tag} (${u.settings.trivia})`; });

		msg.responder.info(users.join('\n'));
	}

};
