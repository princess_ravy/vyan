const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: '<:rrage:584799513265569795>.',
			cooldown: 10
		});
	}

	async run(msg) {
		const e = this.client.guilds.get('594839291390328832').emojis.firstKey(20);
		return e.forEach(i => msg.react(i));
	}

};
