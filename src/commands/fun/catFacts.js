const { Command } = require('klasa');
const get = require('centra');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['catfact', 'kittenfact'],
			description: 'Let me tell you a misterious cat fact.'
		});
	}

	async run(msg) {
		const { fact } = await get('https://catfact.ninja/fact').send().then(r => r.json());
		return msg.sendMessage(`📢 **Catfact:** *${fact}*`);
	}

};
