const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'Shows the choir.',
			subcommands: true,
			quotedStringSupport: true,
			usage: '[add|remove] [user:str] [soprano|alto|tenor|bass]',
			usageDelim: ' '
		});
	}

	async run(msg) {
		return msg.channel.send({
			embed: {
				author: {
					name: 'The Choir'
				},
				fields: [
					{
						name: 'soprano',
						value: this.client.settings.choir.soprano.length > 0 ? this.client.settings.choir.soprano.join('\n') : 'none',
						inline: true
					},
					{
						name: 'alto',
						value: this.client.settings.choir.alto.length > 0 ? this.client.settings.choir.alto.join('\n') : 'none',
						inline: true
					},
					{
						name: 'tenor',
						value: this.client.settings.choir.tenor.length > 0 ? this.client.settings.choir.tenor.join('\n') : 'none',
						inline: true
					},
					{
						name: 'bass',
						value: this.client.settings.choir.bass.length > 0 ? this.client.settings.choir.bass.join('\n') : 'none',
						inline: true
					}
				],
				color: msg.member.roles.color.color
			}
		});
	}

	async add(msg, [user, identifier]) {
		if (!user) return msg.reactor.error();
		if (identifier !== 'soprano' && identifier !== 'alto' && identifier !== 'tenor' && identifier !== 'bass') return msg.reactor.error();
		if (msg.author.id !== this.client.owner.id) return msg.reactor.error();
		await this.client.settings.update(`choir.${identifier}`, user, { action: 'add' });
		return msg.reactor.success();
	}

	async remove(msg, [user, identifier]) {
		if (!user) return msg.reactor.error();
		if (identifier !== 'soprano' && identifier !== 'alto' && identifier !== 'tenor' && identifier !== 'bass') return msg.reactor.error();
		if (msg.author.id !== this.client.owner.id) return msg.reactor.error();
		await this.client.settings.update(`choir.${identifier}`, user, { action: 'remove' });
		return msg.reactor.success();
	}

};
