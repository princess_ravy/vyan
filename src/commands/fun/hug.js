const { Command } = require('klasa');
const get = require('centra');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'Give someone a hug! Yes. Be nice.',
			usage: '<somebody:user>',
			usageDelim: ' '
		});
	}

	async run(msg, [user]) {
		let self;
		if (user.id === msg.author.id) self = true;
		const { url } = await get('https://nekos.life/api/v2/img/hug').send().then(r => r.json());
		msg.channel.send({
			embed: {
				description: self ? `${user} hugged themselves. How awkward.` : `${msg.author} hugged ${user}. How cute. 💕`,
				image: {
					url
				}
			}
		});
	}

};
