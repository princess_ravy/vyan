const { Command } = require('klasa');
const get = require('centra');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'Pat someone! They\'re a good boy.',
			usage: '<somebody:user>',
			usageDelim: ' '
		});
	}

	async run(msg, [user]) {
		let self;
		if (user.id === msg.author.id) self = true;
		const { url } = await get('https://nekos.life/api/v2/img/pat').send().then(r => r.json());
		msg.channel.send({
			embed: {
				description: self ? `${user} gave themselves a pat. Ok.` : `${msg.author} gave ${user} a pat`,
				image: {
					url: url
				}
			}
		});
	}

};
