const get = require('centra');
const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'When your image is too good.',
			aliases: ['jpeg', 'needsmorejpg', 'jpg'],
			usage: '<image:url>',
			cooldown: 5
		});
	}

	async run(msg, [link]) {
		const { message: img } = await get('https://nekobot.xyz/api/imagegen')
			.query({
				type: 'jpeg',
				url: link
			})
			.send()
			.then(r => r.json());
		return msg.responder.image(img);
	}

};
