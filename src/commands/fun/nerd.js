const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'Shows how much of a nerd you are.',
			usage: '[spaces:int{1,511}]',
			cooldown: 5
		});
	}

	async run(msg, [spaces = 3]) {
		return this.client.commands.get('spacetext').run(msg, [spaces, 'nerd']);
	}

};
