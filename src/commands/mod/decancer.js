const { Command } = require('klasa');
const unidecode = require('unidecode');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			requiredPermissions: ['MANAGE_NICKNAMES'],
			runIn: ['text'],
			description: 'Replaces all non-roman characters in someones username.',
			usage: '<member:member>'
		});
	}

	async run(msg, [member]) {
		const cancer = member.displayName;
		let uncancer = unidecode(cancer);
		if (uncancer.length > 32) uncancer = `${uncancer.substring(0, 29)}...`;
		await member.setNickname(uncancer).catch(() => null);
		if (member.displayName === cancer) return msg.reactor.error();
		return msg.reactor.success();
	}

};
