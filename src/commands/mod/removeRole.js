const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			runIn: ['text'],
			aliases: ['rr'],
			description: 'Remove a role from a user.',
			usage: '<member:membername> <role:rolename>',
			usageDelim: ' ',
			quotedStringSupport: true
		});
	}

	async run(msg, [member, role]) {
		await member.roles.remove(role.id);
		return msg.reactor.success();
	}

};
