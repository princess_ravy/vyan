const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['tickets'],
			runIn: ['text'],
			description: 'Manage tickets.',
			usage: '<view|close|list> [id:int]',
			usageDelim: ' ',
			subcommands: true
		});
	}

	async view(msg, [id]) {
		const tickets = msg.guild.settings.get('tickets');
		if (!id || id > tickets.length) return msg.reactor.error();

		return msg.channel.send({ embed: {
			title: `Ticket ${id} (${tickets[id - 1].author})`,
			description: tickets[id - 1].content
		} });
	}

	async close(msg, [id]) {
		const tickets = msg.guild.settings.get('tickets');
		if (!id || id > tickets.length) return msg.reactor.error();
		tickets[id - 1].open = false;

		await msg.guild.channels.get(msg.guild.settings.ticketChannel).send(`Ticket ${id} closed by ${msg.author.tag}`);

		await msg.guild.settings.sync();
		return msg.reactor.success();
	}

	async list(msg) {
		const tickets = [];
		let i = 1;
		for (const ticket of msg.guild.settings.tickets) {
			tickets.push(`${i} - ${ticket.open ? 'open' : 'closed'}`);
			i++;
		}
		return msg.channel.send({ embed: {
			title: `Tickets on ${msg.guild.name}`,
			description: tickets.join('\n')
		} });
	}

};
