const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			requiredPermissions: ['MANAGE_NICKNAMES'],
			runIn: ['text'],
			description: 'Make someone with a nickname with a leading ! or similar appear at the bottom instead of the top.',
			usage: '<member:member>'
		});
	}

	async run(msg, [member]) {
		const hoist = member.displayName;
		let dehoist = `⬇${member.displayName}`;
		if (dehoist.length > 32) dehoist = `${dehoist.substring(0, 29)}...`;
		await member.setNickname(dehoist).catch(() => null);
		if (member.displayName === hoist) return msg.reactor.error();
		return msg.reactor.success();
	}

};
