const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['ub'],
			requiredPermissions: ['BAN_MEMBERS'],
			runIn: ['text'],
			description: 'Unbans a user by id.',
			usage: '<member:user> [reason:string] [...]',
			usageDelim: ' '
		});
	}

	async run(msg, [user, ...reason]) {
		if (user.id === msg.author.id) return msg.responder.error('I don\'t think you\'re banned.');
		if (user.id === this.client.user.id) return msg.responder.error('Umm, why would you unban me?');
		// if ((!reason || !reason.length) && msg.guild.settings.get('reasons')) return msg.responder.error('Reasons are required by the administration on this server.');

		reason = reason.length ? reason.join(' ') : 'no reason specified';
		reason = `${msg.author.tag} | ${reason}`;

		await msg.guild.members.unban(user, reason);
		await msg.guild.logger.unban({
			user,
			reason,
			moderator: msg.author
		});
		return msg.reactor.success();
	}

};
