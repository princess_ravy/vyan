const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'Initialize the purge.',
			runIn: ['text'],
			usage: '<initialize|start> [channel:channel] [text_to_stay:str] [...]',
			usageDelim: ' ',
			subcommands: true
		});
	}

	async initialize(msg, [channel, ...textArr]) {
		if (!channel || !textArr || !textArr.length) return msg.reactor.error();
		const text = textArr.join(' ');
		for (const [id, member] of msg.guild.members) {
			if (!member.user.bot) msg.guild.settings.update('thepurge.kickThese', id, { action: 'add' });
		}
		await msg.guild.settings.update('thepurge.purgeChannel', channel);
		await msg.guild.settings.update('thepurge.textToStay', text);
		await msg.guild.settings.update('thepurge.started', true);
		return msg.responder.info(
			`The Purge has been initialized.`,
			`Users will have to write \`${text}\` in ${channel} to stay.`);
	}

	async start(msg) {
		let kicked = [];
		for (const id of msg.guild.settings.thepurge.kickThese) {
			const member = msg.guild.members.get(id);
			if (!member) continue;
			if (member.kickable) await member.kick(`Kicked during the purge started by ${msg.author.tag}.`).then(_member => kicked.push(_member.user.tag)).catch(() => null);
		}
		await msg.reactor.success();
		await msg.guild.settings.reset('thepurge.started');
		await msg.guild.settings.reset('thepurge.kickThese');
		await msg.guild.settings.reset('thepurge.purgeChannel');
		await msg.guild.settings.reset('thepurge.textToStay');
		kicked = kicked ? kicked.join('\n') : 'none';
		return msg.responder.info(
			'Members kicked duringw the purge:',
			kicked
		).catch(() => msg.responder.error('List of kicked members too large to display.'));
	}

};
