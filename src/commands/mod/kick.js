const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['k'],
			requiredPermissions: ['KICK_MEMBERS'],
			runIn: ['text'],
			description: 'Kicks a mentioned user.',
			usage: '<member:member> [reason:string] [...]',
			usageDelim: ' '
		});
	}

	async run(msg, [member, ...reason]) {
		if (member.id === msg.author.id) return msg.responder.error('Why would you kick yourself?');
		if (member.id === this.client.user.id) return msg.responder.error('Have I done something wrong?');
		if (member.roles.highest.position >= msg.member.roles.highest.position && msg.author.id !== msg.guild.owner.id) return msg.responder.error('You cannot kick this user.');
		if (member.kickable === false) return msg.responder.error('I cannot kick this user.');
		reason = reason.length > 0 ? reason.join(' ') : 'no reason specified';
		reason = `${msg.author.tag} | ${reason}`;
		await member.kick(reason);
		await msg.guild.logger.kick({
			user: member.user,
			reason,
			moderator: msg.author
		});
		return msg.reactor.kick();
	}

};
