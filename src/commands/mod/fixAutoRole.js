const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			runIn: ['text'],
			description: 'Fixes the autoroles in a given channel.',
			usage: '[channel:channel]',
			aliases: ['fixar']
		});
	}

	async run(msg, [channel]) {
		if (!channel) ({ channel } = msg);
		channel.messages.fetch();
		return msg.reactor.success();
	}

};
