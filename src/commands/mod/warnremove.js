const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			requiredPermissions: ['BAN_MEMBERS'],
			runIn: ['text'],
			aliases: ['removewarn', 'rwarn', 'warnr'],
			description: 'Removes a warn by ID.',
			usage: '<id:int> [reason:str] [...]',
			usageDelim: ' '
		});
	}

	async run(msg, [id, ...reason]) {
		reason = reason.length ? reason.join(' ') : 'no reason provided';
		if (id > msg.guild.settings.warns.length || id < 1) return msg.reactor.error();
		const user = this.client.users.get(msg.guild.settings.warns[id - 1].userID);
		await msg.guild.logger.unwarn({
			user,
			reason,
			moderator: msg.author
		});
		await msg.guild.settings.update('warns', msg.guild.settings.warns[id - 1], { action: 'remove' });
		return msg.reactor.success();
	}

};
