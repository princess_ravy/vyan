const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			requiredPermissions: ['BAN_MEMBERS'],
			runIn: ['text'],
			aliases: ['listwarns', 'warns'],
			description: 'List the warns of the current guild.'
		});
	}

	async run(msg) {
		const { warns } = msg.guild.settings;
		if (!warns.length) return msg.responder.info(`No warned users on ${msg.guild.name}`);
		return msg.responder.info(`Warned users on ${msg.guild.name}:`,
			msg.guild.settings.warns
				.map(warn => `${warns.indexOf(warn) + 1}. ${warn.usertag} ${warn.reason}`)
				.join('\n'));
	}

};
