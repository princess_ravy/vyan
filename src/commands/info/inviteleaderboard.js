const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['invlb'],
			runIn: ['text'],
			description: 'Shows top inviters for this server.'
		});
	}

	async run(msg) {
		const _members = msg.guild.members.filter(mem => mem.settings.invites.used > 0);
		if (!_members.size) return msg.responder.error('There have no stored invites been used yet.');
		let i = 0;
		const top10 = _members
			.sort((a, b) => b.settings.invites.used - a.settings.invites.used)
			.first(10)
			.map(item => {
				i++;
				return `#${i} ${item.displayName} (${item.settings.invites.used} invites)`;
			})
			.join('\n');
		return msg.send(
			{
				embed: {
					title: 'Top 10 inviters',
					description: top10
				}
			}
		);
	}

};
