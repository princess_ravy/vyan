const { Command, Duration } = require('klasa');
const { version: discordVersion, MessageEmbed } = require('discord.js');
const { version } = require('../../../package.json');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			guarded: true,
			description: language => language.get('COMMAND_STATS_DESCRIPTION')
		});
	}

	async run(message) {
		const embed = new MessageEmbed()
			.setAuthor(`${this.client.user.username} v${version}`, this.client.user.avatarURL({ format: 'png', size: 256 }))
			.addField('process', [
				`${(process.memoryUsage().heapUsed / 1024 / 1024).toFixed(2)}MB of RAM used`,
				`${Duration.toNow(Date.now() - (process.uptime() * 1000))} Uptime`
			].join('\n'))
			.addField('statistics', [
				`${this.client.users.size.toLocaleString()} Users`,
				`${this.client.guilds.size.toLocaleString()} Guilds`,
				`${this.client.channels.size.toLocaleString()} Channels`
			].join('\n'))
			.addField('dependencies', [
				`Discord.js v${discordVersion}`,
				`Node.js ${process.version}`
			].join('\n'))
			.setColor(message.guild ? message.guild.me.displayColor : 'RANDOM');
		return message.send({ embed });
	}

};
