const { Command } = require('klasa');
const { MessageEmbed } = require('discord.js');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			guarded: true,
			description: 'Get a list of donators.'
		});
	}

	async run(msg) {
		/* eslint-disable-next-line consistent-this */
		const _this = this;
		const embed = new MessageEmbed()
			.setTitle('Thanks for supporting this project:')
			.setDescription(this.client.settings.donators.map(id => _this.client.users.get(id)).map(user => user.tag))
			.setColor(0xFFA000);

		return msg.send({ embed });
	}

};
