const { Command } = require('klasa');
const get = require('centra');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			permissionLevel: 0,
			description: 'Looking for some neat lewd images?',
			nsfw: true
		});
	}

	async run(msg) {
		const { url } = await get('https://nekos.life/api/v2/img/lewd').send().then(r => r.json());
		msg.responder.image(url);
	}

};
