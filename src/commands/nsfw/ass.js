const get = require('centra');
const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'Looking for some neat ass images?',
			nsfw: true
		});
	}

	async run(msg) {
		const { message: img } = await get('https://nekobot.xyz/api/image?type=ass').send().then(r => r.json());
		return msg.responder.image(img);
	}

};
