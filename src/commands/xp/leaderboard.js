const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'Displays the experience leaderboard.',
			aliases: ['lb', 'xplb', 'xpleaderboard', 'top'],
			cooldown: 15
		});
	}

	async run(msg) {
		const leaderboard = [];
		const message = [];
		for (let i = 0; i < this.client.users.size; i++) {
			if (this.client.users.array()[i].settings.experience > 0) {
				leaderboard.push({
					name: this.client.users.array()[i].username,
					experience: this.client.users.array()[i].settings.experience,
					level: this.client.users.array()[i].settings.level
				});
			}
		}
		leaderboard.sort((a, b) => a.experience > b.experience ? -1 : 1);
		for (let j = 0; j < leaderboard.length; j++) {
			message.push(`#${j + 1} ${leaderboard[j].name} (Level ${leaderboard[j].level} - ${leaderboard[j].experience} xp)`);
		}
		msg.channel.send({
			embed: {
				author: {
					name: `Experience leaderboard`,
					iconURL: 'https://i.imgur.com/paCTH6T.png'
				},
				fields: [
					{
						name: 'top 10',
						value: message.slice(0, 10).join('\n')
					}
				]
			}
		});
	}

};
