const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'Give someone credits because you\'re nice.',
			usage: '<someone:user> <amount:int>',
			usageDelim: ' ',
			cooldown: 10
		});
	}

	async run(msg, [user, amount]) {
		if (user.id === msg.author.id || user.bot) return msg.reactor.error();
		if (amount > msg.author.settings.credits) return msg.responder.error("You don't have enough credits.");
		await msg.author.settings.update('credits', msg.author.settings.credits - amount);
		await user.settings.update('credits', user.settings.credits + amount);
		await msg.reactor.success();
		return msg.channel.send({ embed: {
			title: 'Transaction successful',
			description: `Your new balance is **${msg.author.settings.credits}** credits.`
		} });
	}

};
