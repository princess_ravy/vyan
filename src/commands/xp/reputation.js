const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'Give reputation to someone.',
			usage: '<user:user>',
			aliases: ['rep', 'giverep'],
			cooldown: 86400
		});
	}

	async run(msg, [user]) {
		if (!user) return msg.reactor.error();
		if (user.equals(msg.author)) return msg.reactor.error();
		await user.settings.update('reputation', user.settings.reputation + 1);
		return msg.reactor.success();
	}

};
