const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'Check your or someone elses balance.',
			aliases: ['money', 'currency', 'cur', 'balance', 'bal'],
			usage: '[someone:user]',
			cooldown: 5
		});
	}

	async run(msg, [user = msg.author]) {
		return msg.channel.send({ embed: {
			description: `${user.id === msg.author.id ? 'You have' : `${user.tag} has`} **${user.settings.credits.toLocaleString()}** credits.`
		} });
	}

};
