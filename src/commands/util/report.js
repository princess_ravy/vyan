const { Command } = require('klasa');
const req = require('centra');
const imageRegex = /https?:\/\/i\.imgur\.com\/(\w+)\.(?:jpg|png)/i;
const albumRegex = /https?:\/\/imgur\.com\/a\/(\w+)/i;
const generalRegex = /https?:\/\/\S+/i;
const IMGUR_API = 'https://api.imgur.com/3';

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			requiredPermissions: ['SEND_MESSAGES', 'VIEW_CHANNEL', 'READ_MESSAGE_HISTORY', 'EMBED_LINKS'],
			runIn: ['text', 'dm'],
			description: 'Reports a user.',
			usage: '<user:user|username:username>',
			cooldown: 30
		});
	}

	async run(msg, [user]) {
		const { client } = this;
		if (msg.author.settings.reportBlacklisted) { return msg.responder.error(`You have been blacklisted from creating reports. If you believe this is in error, contact ${this.client.owner.tag}.`); }
		const report = {};
		report.user = user.id;
		report.requester = msg.author.id;
		const c1 = await this.ask(`📝 Please provide a reason for reporting \`${user.tag}\` below.`, msg);
		c1.on('collect', m => {
			report.reason = m.content;
			c1.stop();
		});
		c1.on('end', async () => {
			if (!report.reason) return msg.responder.error("Didn't provide valid reason in time.");
			const c2 = await this.ask('📎 Please respond with a valid image proof below.', msg);
			c2.on('collect', async (m) => {
				if (imageRegex.test(m.content)) {
					const res = await req(IMGUR_API)
						.path('/image/')
						.path(imageRegex.exec(m.content)[1])
						.header('Authorization', `Client-ID ${process.env.API_IMGUR_CLIENT_ID}`)
						.send()
						.then(r => r.json());
					if (res.success) {
						report.proof = m.content;
						c2.stop();
					} else {
						msg.channel.send('❌ Invalid link').then(mes => mes.delete({ timeout: 3000 }));
					}
				} else if (albumRegex.test(m.content)) {
					const res = await req(IMGUR_API)
						.path('/album/')
						.path(albumRegex.exec(m.content)[1])
						.path('/images')
						.header('Authorization', `Client-ID ${process.env.API_IMGUR_CLIENT_ID}`)
						.send()
						.then(r => r.json());
					if (res.success && res.data[0]) {
						report.proof = res.data[0].link;
						c2.stop();
					} else {
						msg.channel.send('❌ Invalid link').then(mes => mes.delete({ timeout: 3000 }));
					}
				} else if (generalRegex.test(m.content)) {
					const res = await req(IMGUR_API, 'POST')
						.path('/image')
						.header('Authorization', `Client-ID ${process.env.API_IMGUR_CLIENT_ID}`)
						.body({ image: generalRegex.exec(m.content)[0] })
						.send()
						.then(r => r.json());
					if (res.success) {
						report.proof = res.data.link;
						c2.stop();
					} else {
						console.log(res);
						msg.channel.send('❌ Invalid link').then(mes => mes.delete({ timeout: 3000 }));
					}
				} else if (m.attachments && m.attachments.first() && m.attachments.first().attachment) {
					const res = await req(IMGUR_API, 'POST')
						.path('/image')
						.header('Authorization', `Client-ID ${process.env.API_IMGUR_CLIENT_ID}`)
						.body({ image: m.attachments.first().attachment })
						.send()
						.then(r => r.json());
					if (res.success) {
						report.proof = res.data.link;
						c2.stop();
					} else {
						msg.channel.send('❌ Error with file').then(mes => mes.delete({ timeout: 3000 }));
					}
				} else {
					msg.channel.send('❌ Invalid image').then(mes => mes.delete({ timeout: 3000 }));
				}
			});
			c2.on('end', async () => {
				if (!report.proof) return msg.responder.error("Didn't provide valid proof in time.");
				report.id = this.client.generateSnowflake();
				client.emit('report', report);
				return msg.responder.success(`Thanks for helping us in keeping Discord a safe and sound place! Your report ID is \`${report.id}\`.`);
			});
		});
	}

	async ask(text, msg) {
		const filter = m => m.author.id === msg.author.id;
		await msg.channel.send({ embed: { description: text } });
		return msg.channel.createMessageCollector(filter, { time: 120000 });
	}

};
