/* eslint-disable camelcase */
const get = require('centra');
const baseURL = 'https://od-api.oxforddictionaries.com/api/v1/entries/en/';

const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['def'],
			description: 'Define an english word. Powered by the Oxford Dictionary.',
			usage: '<word:str>'
		});
	}

	async run(msg, [word]) {
		const ctx = await get(baseURL)
			.path(encodeURIComponent(word))
			.header({ app_id: process.env.API_OXFORD_APP_ID, app_key: process.env.API_OXFORD_APP_KEY })
			.send()
			.catch(() => null);
		if (!ctx || !ctx.body || !ctx.body.results) return msg.responder.error('Unknown word');
		const res = ctx.body.results[0];
		const definitions = res.lexicalEntries
			.map(obj => [
				`\`[${obj.lexicalCategory}]\` **${obj.text}** (${obj.pronunciations[0].phoneticSpelling})`,
				`${obj.entries[0].senses[0].definitions
					? obj.entries[0].senses[0].definitions[0]
					: obj.entries[0].senses[0].short_definitions[0]}`,
				obj.entries[0].senses[0].examples.map(ex => `_- ${ex.text}_`).join('\n')
			].join('\n'));
		return msg.responder.info(`${res.lexicalEntries.length} result${res.lexicalEntries.length !== 1 ? 's' : ''} for ${res.id}:`, definitions.join('\n\n'));
	}

};
