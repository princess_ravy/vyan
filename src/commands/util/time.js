const { Command, Timestamp } = require('klasa');
const get = require('centra');
const baseURL = 'http://api.timezonedb.com/v2.1';

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			guarded: true,
			cooldown: 4,
			description: 'Display time in different timezones.',
			usage: '[time:time] <zone:str{3,5}> [...]',
			usageDelim: ' '
		});
		this.timestamp = new Timestamp('HH:mm');
	}

	async run(msg, [time, ...zones]) {
		if (zones.length > 5) return msg.responder.error('Maximum of 5 timezones allowed per request.');
		const message = [];
		for (const zone of zones) {
			const { formatted } = await get(baseURL)
				.path('/get-time-zone')
				.query({
					key: process.env.API_TIMEZONE,
					format: 'json',
					by: 'zone',
					time: time
						? Math.round(time.getTime() / 1000)
						: '',
					zone: encodeURIComponent(zone.toUpperCase())
				})
				.send()
				.then(r => r.json());
			if (!formatted) continue;
			message.push(`\`${zone.toUpperCase()}\` ${this.timestamp.display(formatted.substring(0, 16))}`);
		}
		if (!message.length) return msg.responder.error(`Invalid timezone${zones.length > 1 ? 's' : ''}.`);
		return msg.responder.info(message.join('\n'));
	}

};
