const { Command } = require('klasa');
const { MessageEmbed } = require('discord.js');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['q'],
			runIn: ['text'],
			description: 'Quote a message.',
			usage: '[channel:channel] <message_id:string{17,18}>'
		});
	}

	async run(msg, [channel, id]) {
		const ms = (channel ? channel : msg.channel).messages;
		const m = ms.has(id) ? ms.get(id) : await ms.fetch(id);
		if (!m) throw 'Invalid message ID.';
		const embed = new MessageEmbed()
			.setAuthor(m.author.tag, m.author.displayAvatarURL())
			.setColor(m.member.displayColor || m.guild.me.displayColor || 'RANDOM')
			.setDescription(m.content)
			.setFooter(`${m.guild.name} | #${m.channel.name}`)
			.setTimestamp(m.editedTimestamp || m.createdTimestamp);
		if (m.attachments.size) embed.addField('attachments', m.attachments.map(a => a.url).join('\n'))
		return msg.send({ embed });
	}


};
