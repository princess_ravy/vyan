const { Command } = require('klasa');
const inviteRegex = /(?:https?:\/\/)?(?:www\.)?(?:discord\.gg|discordapp\.com\/invite)?(?:\/)?(?:\s)?(.+)/;

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['inv', 'unkick'],
			runIn: ['text'],
			description: 'Generate invites for a server.',
			subcommands: true,
			usage: '<create|revoke|list> [invite:str]',
			usageDelim: ' '
		});
	}

	async create(msg) {
		const infinite = msg.guild.settings.infiniteUsers.includes(msg.author.id) || msg.guild.settings.infiniteRoles.some(role => msg.member.roles.has(role));
		const { left } = msg.member.settings.invites;
		if (!(infinite || left > 0)) return msg.responder.error("You don't have any invites left.");
		const channel = await msg.guild.channels.get(msg.guild.settings.inviteChannel) || msg.channel;
		const invite = await channel.createInvite({
			maxAge: 0,
			maxUses: 1,
			unique: true,
			reason: `Invite created by ${msg.author.tag}`
		}).catch(() => null);
		if (!invite) return msg.responder.error("I don't have permission to create invites.");
		await msg.member.settings.update('invites.created', invite.code, { action: 'add' });
		await msg.guild.settings.update('invites', { code: invite.code, inviter: msg.member.id }, { action: 'add' });
		if (!infinite) await msg.member.settings.update('invites.left', left - 1);
		await msg.reactor.success();
		return msg.author.responder.success(`Your requested invite to ${msg.guild.name}: ${invite.url}`);
	}

	async revoke(msg, [invite]) {
		if (!inviteRegex.test(invite)) return msg.responder.error('Invalid invite code');
		const code = inviteRegex.exec(invite)[1];
		if (!msg.member.settings.invites.created.includes(code)) return msg.responder.error("That invite doesn't exist in your invites.");
		const currentInvs = await msg.member.guild.fetchInvites();
		if (currentInvs.has(code)) currentInvs.get(code).delete();
		await msg.member.settings.update('invites.created', code, { action: 'remove' });
		await msg.guild.settings.update('invites', { code, inviter: msg.member.id }, { action: 'remove' });
		await msg.reactor.success();
		return msg.responder.success(`Successfully revoked invite \`${code}\``);
	}

	async list(msg) {
		const { created } = msg.member.settings.invites;
		if (!created.length) return msg.responder.error("You don't have any active invites.");
		return msg.author.send({ embed: {
			title: `Your invites to **${msg.guild.name}**:`,
			description: `https://discord.gg/${created.join('\nhttps://discord.gg/')}`
		} });
	}

};
