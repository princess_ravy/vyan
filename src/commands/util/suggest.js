const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			requiredPermissions: ['SEND_MESSAGES', 'VIEW_CHANNEL', 'READ_MESSAGE_HISTORY', 'EMBED_LINKS'],
			runIn: ['text'],
			cooldown: 60,
			aliases: ['sug', 'suggestion'],
			description: 'Suggests something to the moderation of this server.',
			usage: '[suggestion:string] [...]',
			usageDelim: ' '
		});
	}

	async run(msg, [...suggestion]) {
		const suggestionChannel = msg.guild.settings.get('suggestionChannel');
		if (!suggestionChannel) return msg.responder.error("This guild doesn't have a suggestion-channel set up.");
		await msg.guild.channels.get(suggestionChannel).send({
			embed: {
				author: {
					name: msg.author.tag,
					iconURL: msg.author.avatarURL({ format: 'png' })
				},
				color: msg.member.roles.color.color,
				timestamp: Date.now(),
				description: suggestion.join(' ')
			}
		})
			.then(message => message.react('👍')
				.then(reaction => reaction.message.react('👎')));
		return msg.reactor.success();
	}

};
