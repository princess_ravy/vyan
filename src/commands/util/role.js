const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['sar'],
			runIn: ['text'],
			description: 'Assign or unassign roles to yourself.',
			usage: '<get|remove|list> [rolename:rolename]',
			usageDelim: ' ',
			subcommands: true
		});
	}

	async get(msg, [role]) {
		const { selfRoles } = msg.guild.settings;
		if (!selfRoles.includes(role.id)) return msg.responder.error('A self-assignable role by that name could not be found.');
		await msg.member.roles.add(role.id);
		return msg.reactor.success();
	}

	async remove(msg, [role]) {
		const { selfRoles } = msg.guild.settings;
		if (!selfRoles.includes(role.id)) return msg.responder.error('A self-assignable role by that name could not be found.');
		await msg.member.roles.remove(role.id);
		return msg.reactor.success();
	}

	async list(msg) {
		const { selfRoles } = msg.guild.settings;
		return msg.responder.info(`Self-assignable roles on ${msg.guild.name}`, selfRoles.map(role => msg.guild.roles.get(role).name).sort().join('\n') || 'none');
	}

};
