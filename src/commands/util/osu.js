const get = require('centra');
const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'Get OSU! stats for a player!!.',
			usage: '<player:str>',
			cooldown: 5
		});
		this.deprecated = true;
	}

	async run(msg, [player]) {
		const { message: img } = await get(`https://nekobot.xyz/api/imagegen`)
			.query({
				type: 'osu',
				key: process.env.API_OSU,
				username: player
			})
			.send()
			.then(r => r.json());
		return msg.responder.image(img);
	}

};
