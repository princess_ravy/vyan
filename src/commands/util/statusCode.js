/* eslint-disable id-length */
const { Command } = require('klasa');
const request = require('centra');
const cheerio = require('cheerio');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['httpcode', 'httperror'],
			usage: '[link] <statusCode:int{100,599}>',
			usageDelim: ' ',
			description: 'Get a link to the description of an HTTP status code.',
			subcommands: true
		});
	}

	async run(msg, [statusCode]) {
		this.fetch(msg, [statusCode]);
	}

	async link(msg, [statusCode]) {
		return msg.sendMessage(`https://httpstatuses.com/${statusCode}`);
	}

	async fetch(msg, [statusCode]) {
		const res = await request('https://httpstatuses.com/')
			.path(statusCode.toString())
			.send()
			.then(r => r.body);
		const $ = cheerio.load(res);
		return msg.responder.info(
			$('h1').first().text(),
			$('article > p').first().text()
		).catch(() => msg.responder.error("Request invalid. Perhaps that status code doesn't exist?"));
	}

};
