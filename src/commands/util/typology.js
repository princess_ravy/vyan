const { Command } = require('klasa');
// const t = require('../../../typology/types.json')

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['t', 'type', 'types'],
			requiredPermissions: ['SEND_MESSAGES', 'EMBED_LINKS'],
			runIn: ['text'],
			enabled: false,
			description: 'Shows info about all things typology.',
			usage: '<category:str> <type:str>',
			usageDelim: ' '
		});
	}

	async run(msg, [category, type]) {
		return msg.responder.error(`typology/${category}/${type} has not been set up yet.`);
	}

};
