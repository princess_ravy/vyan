const get = require('centra');
const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'Blurple!!',
			usage: '[user:user|link:url]',
			cooldown: 5
		});
	}

	async run(msg, [user]) {
		if (!user) user = msg.author;
		const image = typeof user === 'string' ? user : user.displayAvatarURL({ format: 'png', size: 2048 });
		const { message: img } = await get('https://nekobot.xyz/api/imagegen').query({ type: 'blurpify', image: image }).send().then(r => r.json());
		return msg.responder.image(img);
	}

};
