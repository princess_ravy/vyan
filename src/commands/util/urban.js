const { Command, RichDisplay } = require('klasa');
const get = require('centra');
const { MessageEmbed } = require('discord.js');
const time = 1000 * 60 * 3;

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'Searches the Urban Dictionary library for a definition to the search term.',
			usage: '<search:str>'
		});

		this.handlers = new Map();
	}

	async run(msg, [search]) {
		const body = await get(`http://api.urbandictionary.com/v0/define?term=${encodeURIComponent(search)}`).send().then(r => r.json());
		return this.getDefinition(search, body, msg);
	}

	async getDefinition(search, body, msg) {
		const result = body.list;
		if (!result || !result.length) return msg.channel.send('No entry found.');

		const previousHandler = this.handlers.get(msg.author.id);
		if (previousHandler) previousHandler.stop();

		const handler = await (await this.buildDisplay(msg, body, search, result)).run(await msg.channel.send('Loading Definitions...'), {
			filter: (reaction, user) => user.id === msg.author.id,
			time
		});
		handler.on('end', () => this.handlers.delete(msg.author.id));
		this.handlers.set(msg.author.id, handler);
		return handler;
	}

	async buildDisplay(msg, body, search) {
		const display = new RichDisplay();
		const color = msg.guild ? msg.member.displayColor : 'RANDOM';
		for (const result of body.list) {
			const wdef = result.definition.length > 1000
				? `${this.splitText(result.definition, 1000)}...`
				: result.definition;
			display.addPage(new MessageEmbed()
				.setTitle(search)
				.setURL(result.permalink)
				.addField('Definition', wdef.replace(/\[|\]/g, ''))
				.addField('Example', result.example.length ? result.example.replace(/\[|\]/g, '') : 'none')
				.addField('👍', result.thumbs_up.toString(), true)
				.addField('👎', result.thumbs_down.toString(), true)
				.setFooter(`By ${result.author}`)
				.setColor(color)
			);
		}
		return display;
	}
	splitText(string, length, endBy = ' ') {
		const a = string.substring(0, length).lastIndexOf(endBy);
		const pos = a === -1 ? length : a;
		return string.substring(0, pos);
	}

};
