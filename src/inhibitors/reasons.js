const { Inhibitor } = require('klasa');

module.exports = class extends Inhibitor {

	constructor(...args) {
		super(...args, {
			enabled: true,
			spamProtection: true
		});
	}

	async run(message, command) {
		if (!message.guild || !message.guild.settings.reasons) return;
		const reason = command.usage.parsedUsage.find(tag => tag.possibles[0].name === 'reason');
		if (!reason) return;
		const index = command.usage.parsedUsage.indexOf(reason);
		const args = await message.prompter.validateArgs();
		if (!args[index]) throw 'Reasons are required by the administration on this server.';
	}

};
