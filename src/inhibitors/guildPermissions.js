const { Inhibitor } = require('klasa');
const ALLOWED = 2, NEUTRAL = 1, DENIED = 0;

module.exports = class extends Inhibitor {

	constructor(...args) {
		super(...args, {
			enabled: true
		});
	}

	async run(msg, command) {
		if (!command || !msg.guild) return;
		if (msg.member.hasPermission('ADMINISTRATOR') || msg.author === this.client.owner) return;
		let { category, name } = command;
		category = category.toLowerCase();
		name = name.toLowerCase().replace('-', '');
		if (['Owner', 'Network', 'Testing'].includes(category)) return;
		if (name === 'permissions') {
			throw "Only members with the Administrator permission can edit this server's permissions.";
		}
		const nodes = {
			category,
			command: `${category}.${name}`
		};
		if (await this.getPermissions(msg, nodes)) return;
		throw `You don't have permission to use \`${msg.guild.settings.prefix || this.client.options.prefix}${command.name}\`.`;
	}

	async checkGuild(msg, nodes) {
		const { customPermissions: { allowed, denied } } = msg.guild;
		return this.compare(this.getLowest(allowed, nodes), this.getLowest(denied, nodes));
	}

	async checkRoles(msg, nodes) {
		let allowedR = [], deniedR = [];
		for (const role of msg.member.roles.values()) {
			const { allowed, denied } = role.customPermissions;
			if (allowed.length) allowedR = allowedR.concat(allowed);
			if (denied.length) deniedR = deniedR.concat(denied);
		}
		return this.compare(this.getLowest(allowedR, nodes), this.getLowest(deniedR, nodes));
	}

	async checkMember(msg, nodes) {
		const { customPermissions: { allowed, denied } } = msg.member;
		return this.compare(this.getLowest(allowed, nodes), this.getLowest(denied, nodes));
	}

	async checkChannel(msg, nodes) {
		const { customPermissions: { allowed, denied } } = msg.channel;
		return this.compare(this.getLowest(allowed, nodes), this.getLowest(denied, nodes));
	}

	getLowest(permArray, { command, category }) {
		if (permArray.includes(command)) return 2;
		if (permArray.includes(category)) return 1;
		return 0;
	}

	compare(allowed, denied) {
		if (allowed === 0 && denied === 0) return NEUTRAL;
		if (allowed === denied) return ALLOWED;
		if (allowed > denied) return ALLOWED;
		return DENIED;
	}

	async getPermissions(msg, nodes) {
		const channel = await this.checkChannel(msg, nodes);
		const member = await this.checkMember(msg, nodes);
		const roles = await this.checkRoles(msg, nodes);
		const guild = await this.checkGuild(msg, nodes);

		if (!(channel === NEUTRAL)) return channel;
		if (!(member === NEUTRAL)) return member;
		if (!(roles === NEUTRAL)) return roles;
		if (!(guild === NEUTRAL)) return guild;

		return NEUTRAL;
	}

};
