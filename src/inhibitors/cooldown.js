const { Inhibitor } = require('klasa');
const ms = require('ms');

module.exports = class extends Inhibitor {

	constructor(...args) {
		super(...args, { spamProtection: true });
	}

	async run(message, command) {
		if (message.author === this.client.owner) return;
		if (command.cooldown <= 0) return;

		const existing = command.cooldowns.get(message.levelID);

		if (existing && existing.limited) {
			message.delete().catch(() => null);
			message.responder.error(`Sorry **${message.author.tag}**, you have just used this command. You can use this command again in ${ms(existing.remainingTime, { long: true })}.`)
				.then(msg => msg.delete({ timeout: existing.remainingTime })).catch(() => null);
			throw true;
		}
	}

};
