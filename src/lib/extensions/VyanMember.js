const { Structures } = require('discord.js');

module.exports = Structures.extend('GuildMember', GuildMember => {
	class VyanMember extends GuildMember {

		constructor(...args) {
			super(...args);
			this.customPermissions = this.client.gateways.memberPermissions.create([this.guild.id, this.id]);
		}

		toJSON() {
			return { ...super.toJSON(), customPermissions: this.customPermissions };
		}

	}

	return VyanMember;
});
