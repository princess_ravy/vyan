const { Structures } = require('discord.js');
const GuildLogger = require('../structures/GuildLogger');

module.exports = Structures.extend('Guild', Guild => {
	class VyanGuild extends Guild {

		constructor(...args) {
			super(...args);
			this.customPermissions = this.client.gateways.guildPermissions.create(this.id);
			this.log = this.client.gateways.guildLog.create(this.id);
			this.logger = new GuildLogger(this.client, this);
		}

		toJSON() {
			return { ...super.toJSON(), customPermissions: this.customPermissions, log: this.log };
		}

	}

	return VyanGuild;
});
