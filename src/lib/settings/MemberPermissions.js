const { GatewayStorage, Settings, util: { getIdentifier } } = require('klasa');
const { Collection } = require('discord.js');

class MemberPermissions extends GatewayStorage {

	constructor(store, type, schema, provider) {
		super(store.client, type, schema, provider);
		this.store = store;
		this.syncQueue = new Collection();
		Object.defineProperty(this, '_synced', { value: false, writable: true });
	}

	get Settings() {
		return Settings;
	}

	get idLength() {
		// 18 + 1 + 18: `{MEMBERID}.{GUILDID}`
		return 37;
	}

	get(id) {
		const [guildID, memberID] = typeof id === 'string' ? id.split('.') : id;

		const guild = this.client.guilds.get(guildID);
		if (guild) {
			const member = guild.members.get(memberID);
			return member && member.customPermissions;
		}

		return undefined;
	}

	create(id, data = {}) {
		const [guildID, memberID] = typeof id === 'string' ? id.split('.') : id;
		const entry = this.get([guildID, memberID]);
		if (entry) return entry;

		const customPermissions = new this.Settings(this, { id: `${guildID}.${memberID}`, ...data });
		if (this._synced) customPermissions.sync();
		return customPermissions;
	}

	async sync(input = this.client.guilds.reduce((keys, guild) => keys.concat(guild.members.map(member => member.customPermissions.id)), [])) {
		if (Array.isArray(input)) {
			if (!this._synced) this._synced = true;
			const entries = await this.provider.getAll(this.type, input);
			for (const entry of entries) {
				if (!entry) continue;

				// Get the entry from the cache
				const cache = this.get(entry.id);
				if (!cache) continue;

				cache._existsInDB = true;
				cache._patch(entry);
			}

			// Set all the remaining settings from unknown status in DB to not exists.
			for (const guild of this.client.guilds.values()) {
				for (const member of guild.members.values()) if (member.customPermissions._existsInDB !== true) member.customPermissions._existsInDB = false;
			}
			return this;
		}

		const target = getIdentifier(input);
		if (!target) throw new TypeError('The selected target could not be resolved to a string.');

		const cache = this.get(target);
		return cache ? cache.sync() : null;
	}

}

module.exports = MemberPermissions;
