const { GatewayStorage, Settings, util: { getIdentifier } } = require('klasa');
const { Collection } = require('discord.js');

class GuildLog extends GatewayStorage {

	constructor(store, type, schema, provider) {
		super(store.client, type, schema, provider);
		this.store = store;
		this.syncQueue = new Collection();
		Object.defineProperty(this, '_synced', { value: false, writable: true });
	}

	get Settings() {
		return Settings;
	}

	get(id) {
		const guild = this.client.guilds.get(id);
		return guild && guild.available ? guild.log : undefined;
	}

	create(id, data = {}) {
		const entry = this.get(id);
		if (entry) return entry;

		const log = new this.Settings(this, { id, ...data });
		if (this._synced) log.sync();
		return log;
	}

	async sync(input = this.client.guilds.map(guild => guild.id)) {
		if (Array.isArray(input)) {
			if (!this._synced) this._synced = true;
			const entries = await this.provider.getAll(this.type, input);
			for (const entry of entries) {
				if (!entry) continue;

				// Get the entry from the cache
				const cache = this.get(entry.id);
				if (!cache) continue;

				cache._existsInDB = true;
				cache._patch(entry);
			}

			// Set all the remaining settings from unknown status in DB to not exists.
			for (const guild of this.client.guilds.values()) {
				if (guild.log._existsInDB !== true) guild.log._existsInDB = false;
			}
			return this;
		}

		const target = getIdentifier(input);
		if (!target) throw new TypeError('The selected target could not be resolved to a string.');

		const cache = this.get(target);
		return cache ? cache.sync() : null;
	}

}

module.exports = GuildLog;
