const { GatewayStorage, Settings, util: { getIdentifier } } = require('klasa');
const { Collection } = require('discord.js');

class ChannelPermissions extends GatewayStorage {

	constructor(store, type, schema, provider) {
		super(store.client, type, schema, provider);
		this.store = store;
		this.syncQueue = new Collection();
		Object.defineProperty(this, '_synced', { value: false, writable: true });
	}

	get Settings() {
		return Settings;
	}

	get(id) {
		const channel = this.client.channels.get(id);
		return channel && channel.type === 'text' ? channel.customPermissions : undefined;
	}

	create(id, data = {}) {
		const entry = this.get(id);
		if (entry) return entry;

		const customPermissions = new this.Settings(this, { id, ...data });
		if (this._synced) customPermissions.sync();
		return customPermissions;
	}

	async sync(input = this.client.channels.reduce((channels, channel) => channel.type === 'text' ? channels.concat(channel.id) : channels, [])) {
		if (Array.isArray(input)) {
			if (!this._synced) this._synced = true;
			const entries = await this.provider.getAll(this.type, input);
			for (const entry of entries) {
				if (!entry) continue;

				// Get the entry from the cache
				const cache = this.get(entry.id);
				if (!cache) continue;

				cache._existsInDB = true;
				cache._patch(entry);
			}

			// Set all the remaining settings from unknown status in DB to not exists.
			for (const channel of this.client.channels.values()) {
				if (channel.type === 'text' && channel.customPermissions._existsInDB !== true) channel.customPermissions._existsInDB = false;
			}
			return this;
		}

		const target = getIdentifier(input);
		if (!target) throw new TypeError('The selected target could not be resolved to a string.');

		const cache = this.get(target);
		return cache ? cache.sync() : null;
	}

}

module.exports = ChannelPermissions;
