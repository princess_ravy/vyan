module.exports = {
	CLIENT: {
		gateways: {
			channelPermissions: {},
			guildPermissions: {},
			memberPermissions: {},
			rolePermissions: {},
			guildLog: {}
		},
		providers: {
			default: 'json'
		}
	},
	colors: {
		success: 0x4CAF50,
		error: 0xF44336,
		info: 0x2196F3,
		RED: 0xf44336,
		PINK: 0xE91E63,
		PURPLE: 0x9C27B0,
		DEEP_PURPLE: 0x673AB7,
		INDIGO: 0x3F51B5,
		BLUE: 0x2196F3,
		LIGHT_BLUE: 0x03A9F4,
		CYAN: 0x00BCD4,
		TEAL: 0x009688,
		GREEN: 0x4CAF50,
		LIGHT_GREEN: 0x8BC34A,
		LIME: 0xCDDC39,
		YELLOW: 0xFFEB3B,
		AMBER: 0xFFC107,
		ORANGE: 0xFF9800,
		DEEP_ORANGE: 0xFF5722,
		BROWN: 0x795548,
		GREY: 0x9E9E9E,
		BLUE_GREY: 0x607D8B
	},
	emojis: {
		plus: '<:vPlus:492961647531655178>',
		minus: '<:vMinus:492961643870027796>',
		success: '<:vCheck:492961725201776642>',
		error: '<:vCross:492961655265951744>',
		ban: '<:vHammer:492978447170404353>',
		kick: '👋'
	},
	reactions: {
		success: '492961725201776642',
		error: '492961655265951744',
		ban: '492978447170404353',
		kick: '👋',
		mute: '🔇',
		unmute: '🔊',
		lock: '🔒',
		unlock: '🔓'
	},
	icons: {
		ban: '',
		kick: '',
		mute: '',
		warn: '',
		resetAllNicknames: '',
		unban: '',
		unmute: '',
		unwarn: '',
		purge: '',
		messageDelete: '',
		messageEdit: ''
	}
};
