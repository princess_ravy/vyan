const { Client, Schema, PermissionLevels, util: { mergeDefault } } = require('klasa');
const { CLIENT } = require('./util/constants');
const ChannelPermissions = require('./settings/ChannelPermissions');
const GuildPermissions = require('./settings/GuildPermissions');
const MemberPermissions = require('./settings/MemberPermissions');
const RolePermissions = require('./settings/RolePermissions');
const GuildLog = require('./settings/GuildLog');
const clientOptions = require('../../configs/client');
const { version } = require('../../package.json');
const ImgurClient = require('./structures/ImgurClient');

Client.use(require('klasa-member-gateway'));
Client.use(require('klasa-textchannel-gateway'));
Client.use(require('klasa-dashboard-hooks'));
Client.use(require("@kcp/raw-events"));
require('./Schema');
require('./util/util');

const prefix = require('os').platform() === 'win32' ? process.env.OPTIONS_PREFIX_CANARY : process.env.OPTIONS_PREFIX;
const permissionLevels = new PermissionLevels(11)
	.add(0, () => true)
	.add(1, (client, message) => client.settings.syncedSups.includes(message.author.id))
	.add(2, (client, message) => client.settings.syncedMods.includes(message.author.id))
	.add(3, (client, message) => client.settings.syncedAdmins.includes(message.author.id))
	.add(9, (client, message) => message.author === client.owner, {
		break: true
	})
	.add(10, (client, message) => message.author === client.owner);


class VyanClient extends Client {

	constructor() {
		super({
			// Configuration
			prefix,
			ownerID: process.env.OPTIONS_OWNER_ID,
			slowmode: Number(process.env.OPTIONS_SLOWMODE),
			disabledCorePieces: ['commands'],

			// Discord.js client options
			...clientOptions,

			// Constant variables
			permissionLevels,
			regexPrefix: /^(?:hey)?,? ?(?:vyan,|v!)(?:\s)?/i,
			presence: {
				status: 'online',
				activity: {
					name: `${prefix}help`,
					type: 'LISTENING'
				}
			},
			readyMessage: (_client) =>
				[
					'',
					` ${_client.user.username} v${version}`,
					'',
					`  > Guilds   : ${_client.guilds.size}`,
					`  > Users    : ${_client.users.size}`,
					`  > Prefix   : ${prefix}`,
					''
				].join('\n'),

			// DashboardHooks Config
			dashboardHooks: {
				port: Number(process.env.OPTIONS_API_PORT),
				apiPrefix: '/'
			},
			clientID: process.env.CLIENT_ID,
			clientSecret: process.env.CLIENT_SECRET
		});
		mergeDefault(CLIENT, this.options);
		const { channelPermissions, guildPermissions, memberPermissions, rolePermissions, guildLog } = this.options.gateways;
		const channelPermissionSchema = 'schema' in channelPermissions ? channelPermissions.schema : this.constructor.defaultChannelPermissionSchema;
		const guildPermissionSchema = 'schema' in guildPermissions ? guildPermissions.schema : this.constructor.defaultGuildPermissionSchema;
		const memberPermissionSchema = 'schema' in memberPermissions ? memberPermissions.schema : this.constructor.defaultMemberPermissionSchema;
		const rolePermissionSchema = 'schema' in rolePermissions ? rolePermissions.schema : this.constructor.defaultRolePermissionSchema;
		const guildLogSchema = 'schema' in guildLog ? guildLog.schema : this.constructor.defaultGuildLogSchema;

		this.gateways.channelPermissions = new ChannelPermissions(this.gateways, 'channelPermissions', channelPermissionSchema, channelPermissions.provider || this.options.providers.default);
		this.gateways.guildPermissions = new GuildPermissions(this.gateways, 'guildPermissions', guildPermissionSchema, guildPermissions.provider || this.options.providers.default);
		this.gateways.memberPermissions = new MemberPermissions(this.gateways, 'memberPermissions', memberPermissionSchema, memberPermissions.provider || this.options.providers.default);
		this.gateways.rolePermissions = new RolePermissions(this.gateways, 'rolePermissions', rolePermissionSchema, rolePermissions.provider || this.options.providers.default);
		this.gateways.guildLog = new GuildLog(this.gateways, 'guildLog', guildLogSchema, guildLog.provider || this.options.providers.default);

		this.gateways.keys.add('channelPermissions');
		this.gateways.keys.add('guildPermissions');
		this.gateways.keys.add('memberPermissions');
		this.gateways.keys.add('rolePermissions');
		this.gateways.keys.add('guildLog');

		this.gateways._queue.push(this.gateways.channelPermissions.init.bind(this.gateways.channelPermissions));
		this.gateways._queue.push(this.gateways.guildPermissions.init.bind(this.gateways.guildPermissions));
		this.gateways._queue.push(this.gateways.memberPermissions.init.bind(this.gateways.memberPermissions));
		this.gateways._queue.push(this.gateways.rolePermissions.init.bind(this.gateways.rolePermissions));
		this.gateways._queue.push(this.gateways.guildLog.init.bind(this.gateways.guildLog));

		this.imgur = new ImgurClient();
		this.imgur.init();
	}

	get syncedGuilds() {
		const client = this;
		return this.guilds.filter(g => client.settings.syncedGuilds.includes(g.id));
	}

	generateSnowflake () {
		const timestamp = BigInt(new Date().getTime());
		return (((timestamp - 1420070400000n) << 22n) + (BigInt(this.shard || 0) << 12n)).toString();
	}

	async syncBan (user, reason) {
		for (const guild of this.syncedGuilds) {
			guild.members.ban(user, { reason }).catch(() => null);
		}
		return true;
	}

}

module.exports = VyanClient;

VyanClient.defaultChannelPermissionSchema = new Schema()
	.add('allowed', 'string', { array: true })
	.add('denied', 'string', { array: true });
VyanClient.defaultGuildPermissionSchema = new Schema()
	.add('allowed', 'string', { array: true })
	.add('denied', 'string', { array: true, default: ['mod', 'conf'] });
VyanClient.defaultMemberPermissionSchema = new Schema()
	.add('allowed', 'string', { array: true })
	.add('denied', 'string', { array: true });
VyanClient.defaultRolePermissionSchema = new Schema()
	.add('allowed', 'string', { array: true })
	.add('denied', 'string', { array: true });
VyanClient.defaultGuildLogSchema = new Schema()
	.add('channel', 'textchannel')
	.add('webhook', 'string')
	.add('logs', 'any', { array: true });
