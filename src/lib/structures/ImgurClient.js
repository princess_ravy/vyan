/* eslint-disable camelcase */
const req = require('centra');
const BASE_URL = 'https://api.imgur.com';

module.exports = class ImgurClient {

	constructor(refreshToken, clientId, clientSecret) {
		this.refreshToken = refreshToken || process.env.API_IMGUR_TOKEN_REFRESH;
		this.accessToken = null;
		this.clientId = clientId || process.env.API_IMGUR_CLIENT_ID;
		this.clientSecret = clientSecret || process.env.API_IMGUR_CLIENT_SECRET;
	}

	async generateAccessToken() {
		return req(BASE_URL, 'POST')
			.path('/oauth2/token')
			.body({
				refresh_token: this.refreshToken,
				client_id: this.clientId,
				client_secret: this.clientSecret,
				grant_type: 'refresh_token'
			}, 'form')
			.send()
			.then(res => res.json().access_token);
	}

	async init() {
		this.access_token = await this.generateAccessToken();
	}

};
