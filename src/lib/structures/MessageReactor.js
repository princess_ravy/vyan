const { reactions } = require('../util/constants');

module.exports = class MessageReactor {

	constructor(message) {
		this.message = message;
	}

	success() {
		return this.message.react(reactions.success);
	}

	error() {
		return this.message.react(reactions.error);
	}

	ban() {
		return this.message.react(reactions.ban);
	}

	kick() {
		return this.message.react(reactions.kick);
	}

	mute() {
		return this.message.react(reactions.mute);
	}

	unmute() {
		return this.message.react(reactions.unmute);
	}

	lock() {
		return this.message.react(reactions.lock);
	}

	unlock() {
		return this.message.react(reactions.unlock);
	}

};
