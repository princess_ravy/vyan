const { MessageEmbed } = require('discord.js');
const { colors, icons } = require('../util/constants');

module.exports = class GuildLogger {

	constructor(client, guild) {
		this.client = client;
		this.guild = guild;
	}

	async send({ action, user, color, reason, moderator, previous, now, text, avatarURL }) {
		if (!this.logChannel) this.logChannel = this.client.channels.get(this.guild.log.channel);
		if (!this.webhook) this.webhook = this.logChannel ? await this.logChannel.fetchWebhooks().then(hooks => hooks.get(this.guild.log.webhook)) : null;
		const embed = new MessageEmbed()
			.setAuthor(`${user.tag}`, user.avatarURL())
			.setColor(colors[color])
			.setFooter(`id: ${user.id}`);
		if (reason) embed.addField('Reason', reason, true);
		if (moderator) embed.addField('Moderator', `${moderator} (${moderator.tag})`, true);
		if (previous) embed.addField('Previous', previous, true);
		if (now) embed.addField('Now', now, true);
		if (text) embed.setDescription('text');
		return this.webhook.send({ username: action, avatarURL, embeds: [embed] });
	}

	ban({ user, moderator, reason, time, soft, sync }) {
		let action = time ? 'Tempban' : soft ? 'Softban' : 'Ban';
		if (sync) action += ' (global)';
		const avatarURL = icons.ban;
		this.send({ action, user, moderator, reason, color: 'RED', avatarURL });
	}

	kick({ user, moderator, reason, sync }) {
		let action = 'Kick';
		if (sync) action += ' (global)';
		const avatarURL = icons.kick;
		this.send({ action, user, moderator, reason, color: 'AMBER', avatarURL });
	}

	mute({ user, moderator, reason, time, sync }) {
		let action = time ? 'Tempmute' : 'Mute';
		if (sync) action += ' (global)';
		const avatarURL = icons.mute;
		this.send({ action, user, moderator, reason, color: 'AMBER', avatarURL });
	}

	warn({ user, moderator, reason }) {
		const action = 'Warn';
		const avatarURL = icons.warn;
		this.send({ action, user, moderator, reason, color: 'AMBER', avatarURL });
	}

	resetAllNicknames({ reason, moderator }) {
		const action = 'Reset All Nicknames';
		const avatarURL = icons.resetAllNicknames;
		this.send({ action, user: moderator, reason, color: 'BLUE', avatarURL });
	}

	unban({ user, moderator, reason, sync }) {
		let action = 'Unban';
		if (sync) action += ' (global)';
		const avatarURL = icons.unban;
		this.send({ action, user, moderator, reason, color: 'GREEN', avatarURL });
	}

	unmute({ user, moderator, reason, sync }) {
		let action = 'Unmute';
		if (sync) action += ' (global)';
		const avatarURL = icons.unmute;
		this.send({ action, user, moderator, reason, color: 'GREEN', avatarURL });
	}

	unwarn({ user, moderator, reason }) {
		const action = 'Warn Removed';
		const avatarURL = icons.unwarn;
		this.send({ action, user, moderator, reason, color: 'GREEN', avatarURL });
	}

	purge({ moderator, part }) {
		const action = `Purge ${part}`;
		const avatarURL = icons.purge;
		this.send({ action, user: moderator, color: 'RED', avatarURL });
	}

	messageDelete({ user, previous }) {
		const action = 'Message Deleted';
		const avatarURL = icons.messageDelete;
		this.send({ action, user, previous, color: 'ORANGE', avatarURL });
	}

	messageEdit({ user, previous, now }) {
		const action = 'Message Edited';
		const avatarURL = icons.messageEdit;
		this.send({ action, user, previous, now, color: 'YELLOW', avatarURL });
	}

	syncBan(args) {
		for (const guild of this.client.syncedGuilds.values()) {
			guild.logger.ban({ ...args, sync: true });
		}
	}

	syncKick(args) {
		for (const guild of this.client.syncedGuilds.values()) {
			guild.logger.kick({ ...args, sync: true });
		}
	}

	syncMute(args) {
		for (const guild of this.client.syncedGuilds.values()) {
			guild.logger.mute({ ...args, sync: true });
		}
	}

	syncTempBan(args) {
		for (const guild of this.client.syncedGuilds.values()) {
			guild.logger.ban({ ...args, sync: true });
		}
	}

	syncTempMute(args) {
		for (const guild of this.client.syncedGuilds.values()) {
			guild.logger.mute({ ...args, sync: true });
		}
	}

	syncUnban(args) {
		for (const guild of this.client.syncedGuilds.values()) {
			guild.logger.unban({ ...args, sync: true });
		}
	}

	syncUnmute(args) {
		for (const guild of this.client.syncedGuilds.values()) {
			guild.logger.unmute({ ...args, sync: true });
		}
	}

};
