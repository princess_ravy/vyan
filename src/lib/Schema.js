const { Client } = require('klasa');

Client.defaultClientSchema
	// Syncing
	.add('syncedGuilds', 'string', { array: true, default: [] })
	.add('syncBannedUsers', 'any', { array: true, default: [], configurable: false })
	.add('syncedSups', 'User', { array: true, default: [], configurable: false })
	.add('syncedMods', 'User', { array: true, default: [], configurable: false })
	.add('syncedAdmins', 'User', { array: true, default: [], configurable: false })

	// Choir & Family
	.add('choir', folder => folder
		.add('soprano', 'string', { default: [], array: true })
		.add('alto', 'string', { default: [], array: true })
		.add('tenor', 'string', { default: [], array: true })
		.add('bass', 'string', { default: [], array: true })
	)
	.add('family', folder => folder
		.add('parents', 'string', { default: [], array: true })
		.add('children', 'string', { default: [], array: true })
	)

	// Quiz Questions
	.add('quiz', 'any', { default: [], array: true,	configurable: false	})
	.add('donators', 'User', { default: [], array: true, configurable: false })

	// Shower Thoughts
	.add('showerThoughts', 'any', { default: [], array: true })

	// Error Handling
	.add('errors', 'any', { default: [], array: true, configurable: false });

Client.defaultGuildSchema
	// Important Channels
	.add('announcementChannel', 'Channel', { default: null })
	.add('reportChannel', 'Channel', { default: null })
	.add('starChannel', 'Channel', { default: null })
	.add('suggestionChannel', 'Channel', { default: null })
	.add('ticketChannel', 'Channel', { default: null })
	.add('voteChannel', 'Channel', { default: null })

	// Roles
	.add('autoRole', 'Role', { default: null })
	.add('botRole', 'Role', { default: null })
	.add('muteRole', 'Role', { default: null })
	.add('selfRoles', 'Role', { default: [], array: true, configurable: false })
	.add('reactionRoles', 'any', { default: [], array: true, configurable: false })

	// Logs
	.add('logs', 'any', { array: true, default: [], configurable: false })
	.add('logChannel', 'channel')
	.add('logWebhook', 'string')
	.add('log', folder => folder
		.add('members', 'Channel', { default: null })
		.add('moderation', 'Channel', { default: null })
		.add('messages', 'Channel', { default: null })
	)

	// Mod AddOns
	.add('reasons', 'boolean', { default: false })
	.add('customReactions', 'any', { default: [], array: true })
	.add('levelroles', 'any', { default: [], array: true })
	.add('tickets', 'any', { default: [], array: true })
	.add('warns', 'any', { default: [], array: true, configurable: false })

	// Neat features
	.add('minAccAge', 'integer', { default: 1800000 })
	.add('deleteCommand', 'boolean', { default: false })
	.add('iamResponse', 'boolean', { default: false, configurable: false })

	// AntiSpam
	.add('antispam', folder => folder
		.add('modules', subfolder => subfolder
			.add('anti_invite', 'boolean', { default: false })
			.add('anti_mass_mention', 'boolean', { default: false })
			.add('anti_mass_role_mention', 'boolean', { default: false })
			.add('anti_everyone', 'boolean', { default: false })
			.add('anti_swearings', 'boolean', { default: false })
			.add('anti_repeat', 'boolean', { default: false })
			.add('anti_quick_send', 'boolean', { default: false })
			.add('anti_heuristics', 'boolean', { default: false })
			.add('anti_kys', 'boolean', { default: false })
		)
		.add('settings', subfolder => subfolder
			.add('bannedWords', 'string', { default: [], array: true })
			.add('maxMentions', 'integer', { default: 5 })
			.add('maxRoleMentions', 'integer', { default: 3 })
			.add('maxRepetitions', 'integer', { default: 3 })
			.add('messageInterval', 'integer', { default: 1000 })
		)
		.add('exclusions', subfolder => subfolder
			.add('roles', 'Role', { default: [], array: true })
			.add('channels', 'Channel', { default: [], array: true })
		)
	)

	// Purge
	.add('thepurge', folder => folder
		.add('started', 'boolean', { default: false, configurable: false })
		.add('kickThese', 'string', { default: [], configurable: false, array: true })
		.add('purgeChannel', 'Channel', { default: null, configurable: false })
		.add('textToStay', 'string', { default: '', configurable: false })
	)

	// Welcoming
	.add('welcome', folder => folder
		.add('message', 'string', { configurable: false })
		.add('enabled', 'boolean', { configurable: false, default: false })
		.add('channel', 'channel', { configurable: false })
	)

	// Invite Management
	.add('invites', 'any', { array: true, configurable: false, default: [] })
	.add('inviteChannel', 'Channel')
	.add('infiniteUsers', 'User', { array: true, configurable: false, default: [] })
	.add('infiniteRoles', 'Role', { array: true, configurable: false, default: [] })

	// Starboard
	.add('starred', 'any', { array: true, configurable: false, default: [] })

	// Someone
	.add('disableSomeone', 'boolean', { default: false })

	// Voice
	.add('voice', folder => folder
		.add('temporary', 'VoiceChannel')
		.add('channels', 'any', { default: [], array: true, configurable: false })
	)

	// AutoNick
	.add('nickPrefix', 'string', { default: null })
	.add('nickSuffix', 'string', { default: null })

	// Join security
	.add('security', 'boolean', { default: false })

	// CustomCommands
	.add('customCommands', 'any', { array: true, configurable: false, default: [] })

	// Groovy: hidden channels
	.add('hiddenChannels', 'string', { array: true, configurable: false, default: [] });

Client.defaultMemberSchema
	// Invite Management
	.add('invites', folder => folder
		.add('used', 'integer', { default: 0, configurable: false })
		.add('left', 'integer', { default: 0, configurable: false })
		.add('created', 'string', { default: [], array: true, configurable: false })
		.add('users', 'User', { default: [], array: true, configurable: false })
	)
	.add('muted', 'boolean', { default: false, configurable: false });

Client.defaultUserSchema
	// Marriage
	.add('marriedTo', 'User', { default: null, configurable: false })
	.add('proposals', 'User', {	array: true, default: [], configurable: false })

	// Loyalty
	.add('experience', 'integer', {	default: 0,	configurable: false	})
	.add('level', 'integer', { default: 0, configurable: false })

	// Scores
	.add('credits', 'integer', { default: 0, configurable: false })
	.add('reputation', 'integer', {	default: 0,	configurable: false	})
	.add('quiz', 'integer', { default: 0, configurable: false })

	// Trivia
	.add('trivia', 'integer', { default: 0, configurable: false })

	// AntiSpam
	.add('previous_profanity', 'float', { default: 0, configurable: false })
	.add('kysScolded', 'boolean', { default: false, configurable: false })
	.add('reportBlacklisted', 'boolean', { defaut: false, configurable: false })
	// User Prefix
	.add('prefixes', 'String', { array: true });

Client.defaultTextChannelSchema
	.add('locked', 'boolean', { default: false, configurable: false });
