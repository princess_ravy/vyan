require('dotenv').config();

if (Number(process.env.CLIENT_SHARD_COUNT)) {
	const { ShardingManager } = require('discord.js');
	const shardCount = Number(process.env.CLIENT_SHARD_COUNT);

	const manager = new ShardingManager('./src/vyan.js', {
		totalShards: shardCount,
		respawn: true,
		token: process.env.DISCORD_TOKEN
	});

	manager.spawn(shardCount);
	manager.on('shardCreate', shard => {
		console.log('Master', `Spawning shard ${shard.id}.`);
	});
} else {
	require('./vyan');
}
