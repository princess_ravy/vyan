const { Extendable } = require('klasa');
const { Message } = require('discord.js');
const reactions = new Map()
	.set('a', '🇦')
	.set('b', '🇧')
	.set('c', '🇨')
	.set('d', '🇩')
	.set('e', '🇪')
	.set('f', '🇫')
	.set('g', '🇬')
	.set('h', '🇭')
	.set('i', '🇮')
	.set('j', '🇯')
	.set('k', '🇰')
	.set('l', '🇱')
	.set('m', '🇲')
	.set('n', '🇳')
	.set('o', '🇴')
	.set('p', '🇵')
	.set('q', '🇶')
	.set('r', '🇷')
	.set('s', '🇸')
	.set('t', '🇹')
	.set('u', '🇺')
	.set('v', '🇻')
	.set('w', '🇼')
	.set('x', '🇽')
	.set('y', '🇾')
	.set('z', '🇿')
	.set('0', '0⃣')
	.set('1', '1⃣')
	.set('2', '2⃣')
	.set('3', '3⃣')
	.set('4', '4⃣')
	.set('5', '5⃣')
	.set('6', '6⃣')
	.set('7', '7⃣')
	.set('8', '8⃣')
	.set('9', '9⃣')
	.set('*', '👌')
	.set('+', '✅')
	.set('-', '❌');

module.exports = class extends Extendable {

	constructor(...args) {
		super(...args, { appliesTo: [Message] });
	}

	async addReaction(reaction) {
		reaction = reaction.toLowerCase();
		for (const character of reaction) {
			if (!reactions.has(character)) continue;
			await this.react(reactions.get(character)).catch(() => null);
		}
		return;
	}

};
