const { Argument } = require('klasa');

module.exports = class extends Argument {

	constructor(...args) {
		super(...args, { aliases: ['cmd'] });
	}

	run(arg, possible, message) {
		const command = this.client.commands.get(arg.toLowerCase());
		if (command) return command;
		if (typeof arg === 'string' && ['admin', 'conf', 'fun', 'info', 'mod', 'nsfw', 'util', 'xp'].includes(arg)) return arg;
		throw message.language.get('RESOLVER_INVALID_PIECE', possible.name, 'command');
	}

};
