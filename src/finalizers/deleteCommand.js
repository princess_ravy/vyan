const { Finalizer } = require('klasa');

module.exports = class extends Finalizer {

	async run(msg) {
		if (msg.guild && msg.guild.settings.deleteCommand && msg.deletable) await msg.delete();
	}

};
