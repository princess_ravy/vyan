/* eslint-disable camelcase, complexity */
// Load Klasa
const { Monitor } = require('klasa');
// Load Swearings
const swearingsjson = require('./swearings.json');
const swearingsnewjson = require('./swearingsnew.json');
const swearings = swearingsjson.words;
const swearingsnew = swearingsnewjson.words;
// Initialize AntiSpam stage 4
const interval = 1000;
const authors = [];
const messagelog = [];
// Initialize AntiSpam stage 5
const sajs = require('string-analysis-js');
// Initialize AntiSpam stage 8
const bannedWords = ['kys', 'killyourself', 'killurself', 'killmyself'];

module.exports = class extends Monitor {

	constructor(...args) {
		super(...args, {
			name: 'antispam',
			enabled: true,
			ignoreSelf: true,
			ignoreBots: false,
			ignoreOthers: false
		});
	}

	async run(msg) {
		// STAGE 0: Configure the AntiSpam
		// STAGE 0.1: Check the break conditions and initialize all settings
		if (!msg.guild) return null;
		if (!msg.author.settings.previous_profanity) {
			msg.author.settings.update('previous_profanity', 0);
		}
		if (msg.member.roles.some(role => msg.guild.settings.antispam.exclusions.roles.includes(role.id))) return null;
		if (msg.guild.settings.antispam.exclusions.channels.includes(msg.channel.id)) return null;
		// STAGE 0.2: Define the environment variables
		let valMentionsUsers = 0;
		let valMentionsRoles = 0;

		let valEveryone = 0;

		let valGlobalSwearing = 0;
		let valGuildSwearing = 0;

		let valRepetitionSame = 0;
		let valRepetitionInterval = 0;

		let valAnalysis = 0;
		let valAnalysis1 = 0;
		let valAnalysis2 = 0;
		let valAnalysis3 = 0;
		let valAnalysis4 = 0;
		let valAnalysis5 = 0;
		const valHistory = msg.author.settings.previous_profanity;

		let valInvites = 0;

		let valKys = 0;

		let valFinal = 0;

		// STAGE 1: Check if users or roles are mentioned
		// STAGE 1.1: Check how many users are mentioned; if >=5, then set mass_mention -> = 1
		if (msg.guild.settings.antispam.modules.anti_mass_mention) {
			if (msg.mentions.users.size > msg.guild.settings.antispam.settings.maxMentions) {
				valMentionsUsers = 1;
			}
		}
		// STAGE 1.2: Check how many roles are mentioned; if >=3, then set mass_mention -> = 1
		if (msg.guild.settings.antispam.modules.anti_mass_role_mention) {
			if (msg.mentions.roles.size > msg.guild.settings.antispam.settings.maxRoleMentions) {
				valMentionsRoles = 1;
			}
		}

		// STAGE 2: Check for mentioning @everyone
		if (msg.guild.settings.antispam.modules.anti_everyone) {
			if (msg.mentions.everyone) {
				valEveryone = 1;
			}
		}
		// STAGE 3: Check for swearings on a guild and bot basis
		// STAGE 3.1: Check for the bot's swearing database
		if (msg.guild.settings.antispam.modules.anti_swearings) {
			const msgArray = msg.content.split(' ');

			for (const item of msgArray) {
				if (swearings.includes(item) || swearingsnew.includes(item)) {
					valGlobalSwearing = 1;
				}
			}
		}
		// STAGE 3.2 Check for the guild's swearing database
		for (const item of msg.guild.settings.antispam.settings.bannedWords) {
			if (msg.content.toLowerCase().includes(item.toLowerCase())) {
				valGuildSwearing = 1;
			}
		}

		// STAGE 4: Check for repetition of messages; common definition of "spam"
		// STAGE 4.1 log the messages
		const now = Math.floor(Date.now());
		authors.push({
			time: now,
			author: msg.author.id
		});
		messagelog.push({
			message: msg.content,
			author: msg.author.id
		});
		// STAGE 4.2 Check how many times the same message has been sent.
		if (msg.guild.settings.antispam.modules.anti_repeat) {
			let msgMatch = 0;
			for (let i = messagelog.length - 1; i > 0; i--) {
				if (messagelog[i].message === messagelog[i - 1].message && (messagelog[i].author === messagelog[i - 1].author) && (msg.author.id !== this.client.user.id)) {
					msgMatch++;
				}
			}
			if (msgMatch > msg.guild.settings.antispam.settings.maxRepetitions) {
				valRepetitionSame = 1;
			}
		}
		// STAGE 4.3 Messages in a defined interval
		if (msg.guild.settings.antispam.modules.anti_quick_send) {
			let matched = 0;
			for (let i = 0; i < authors.length; i++) {
				if (authors[i].time > now - interval) {
					matched++;
				}
				if (messagelog.length >= 200) {
					messagelog.shift();
				}
			}
			if (matched > 2) {
				valRepetitionInterval = 1;
			}
		}

		// STAGE 5: Perform string-analysis
		if (msg.guild.settings.antispam.modules.anti_heuristics) {
			const len = msg.content.length;
			// STAGE 5.1 Repetitive structure
			const repetitiveStructure = sajs.getPercentageOfRepetitiveStructure(msg.content);
			if (len > 12 && repetitiveStructure > 0.5) {
				valAnalysis1 = 1;
			} else {
				valAnalysis1 = 0;
			}
			// STAGE 5.2 Unnormally short strings
			const shortStrings = sajs.getPercentageOfShortStrings(msg.content, ' ', 3);
			if (len > 24 && shortStrings > 0.5) {
				valAnalysis2 = 1;
			} else {
				valAnalysis2 = 0;
			}
			// STAGE 5.3 Unnormally long strings
			const longStrings = sajs.getPercentageOfLongStrings(msg.content);
			if (len > 64 && longStrings > 0.5) {
				valAnalysis3 = 1;
			} else {
				valAnalysis3 = 0;
			}
			// STAGE 5.4 Repetitive Characters
			const repetitiveChars = sajs.getPercentageOfRepetitiveChars(msg.content);
			if (len > 8 && repetitiveChars > 0.5) {
				valAnalysis4 = 1;
			} else {
				valAnalysis4 = 0;
			}
			// STAGE 5.5 Caps (Uppercase Characters)
			const upperCaseChars = sajs.getPercentageOfUpperCaseChars(msg.content);
			if (len > 8 && upperCaseChars > 0.5) {
				valAnalysis5 = 1;
			} else {
				valAnalysis5 = 0;
			}
			// STAGE 5.6 Get the modified average of these
			valAnalysis = (valAnalysis1 * 0.8) + (valAnalysis2 * 0.4) + (valAnalysis3 * 0.4) + (valAnalysis4 * 0.8) + (valAnalysis5 * 0.6) + (valHistory / 1000);
		}

		// STAGE 6: Check for invites
		if (msg.guild.settings.antispam.modules.anti_invite) {
			if (/(https?:\/\/)?(www\.)?(discord\.(gg|li|me|io)|discordapp\.com\/invite)\/(\s)?.+/.test(msg.content)) {
				valInvites = 1;
			}
		}

		// STAGE 7: Seperate kys filter
		if (msg.guild.settings.antispam.modules.anti_kys) {
			const cleanMsg = msg.content.toLowerCase().replace(/[^a-z]/gi, '');
			let triggered = false;
			bannedWords.every(word => {
				if (cleanMsg.includes(word)) {
					triggered = true;
					return false;
				} else { return true; }
			});
			if (triggered) {
				valKys = 2;
				if (!msg.author.settings.kysScolded) {
					await msg.author.sendMessage(
						[
							'While the use of the term `kys` or variations may seem innocent to you, please remember that this can truly, and very dangerously, affect some people.',
							'People suffering from depression can see this and essentially be triggered into having suicidal thoughts.',
							'That means **you** could be the cause of a loss of life, and we know you don\'t want that!',
							'We know you may mean it as just a meme, or maybe just as a variation on "go *bleep* yourself".',
							'Adapted from the Discord Community guidelines (<https://discordapp.com/guidelines>):',
							'THE FOLLOWING IS NOT TOLERATED:',
							'Sharing content glorifying or promoting, in general, self-harm or suicide.',
							'This includes any content that encourages others to: cut or injure themselves; embrace anorexia, bulimia, or other eating disorders; or commit suicide rather than, for example, seeking counseling or treatment.',
							'Online communities have the power to lift people up from dark places - be the kind of person who lifts others.',
							'If you notice that someone is in need of urgent help, please contact your local authorities.'
						].join('\n'))
						.catch(() => null);
					await msg.author.settings.update('kysScolded', true);
				}
			}
		}
		// STAGE 8: Do stuff.
		valFinal
			= valMentionsUsers
			+ valMentionsRoles
			+ valEveryone
			+ valGlobalSwearing
			+ valGuildSwearing
			+ valRepetitionSame
			+ valRepetitionInterval
			+ valAnalysis
			+ valInvites
			+ valKys;

		await msg.author.settings.update('previous_profanity', Math.round(valFinal * 100));

		if (valFinal >= 1) {
			return msg.delete().catch(() => null);
		} else {
			return null;
		}
	}

};
