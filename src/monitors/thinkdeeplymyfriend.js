const { Monitor } = require('klasa');

module.exports = class extends Monitor {

	constructor(...args) {
		super(...args, {
			enabled: true,
			ignoreWebhooks: false,
			ignoreBots: false,
			ignoreOthers: false
		});
	}

	async run(msg) {
		if (msg.channel.id !== '477189578168729621') return;
		await msg.react('477133343356616731');
		await msg.react('477153007172321294');
		return;
	}

};
