const { Monitor, util: { regExpEsc } } = require('klasa');
const { Parser } = require('breadtags');

module.exports = class extends Monitor {

	constructor(...args) {
		super(...args, {
			name: 'customCommands',
			enabled: true,
			ignoreSelf: false,
			ignoreBots: true,
			ignoreOthers: false
		});
	}

	async run(msg) {
		if (msg.channel.type !== 'text') return null;
		const res = await this.checkCommand(msg);
		if (!res) return null;
		const { name, command: cmd } = res;
		const args = msg.content.replace(new RegExp(regExpEsc(`${name} ?`), '')).split(' ');
		const { author: user, member, channel, guild } = msg;
		const message = await new Parser().parse(cmd, {
			user, member, channel, guild, args
		});
		if (message && message.length) return msg.send(message);
	}

	async checkCommand(msg) {
		for (const { name, command } of msg.guild.settings.customCommands) {
			if (msg.content.toLowerCase().startsWith(`${name} `) || msg.content.toLowerCase() === name) return { name, command };
		}
		return null;
	}

};
