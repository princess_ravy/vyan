const { Monitor } = require('klasa');
const iamReg = /^(?:I|i)(?:'?m| am) ([a-z]+)/i;

module.exports = class extends Monitor {

	constructor(...args) {
		super(...args, {
			enabled: true,
			ignoreSelf: false,
			ignoreBots: true,
			ignoreOthers: false
		});
	}

	async run(msg) {
		if (msg.guild && !msg.guild.settings.iamResponse) return;
		const res = iamReg.exec(msg.content);
		if (!res) return;
		msg.send(`Hi ${res[1]}, I'm ${this.client.user.username}!`);
	}

};
