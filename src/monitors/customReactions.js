const { Monitor } = require('klasa');

module.exports = class extends Monitor {

	constructor(...args) {
		super(...args, {
			name: 'customReactions',
			enabled: true,
			ignoreSelf: true,
			ignoreBots: true,
			ignoreOthers: false
		});
	}

	async run(msg) {
		if (!msg.guild) return null;
		for (let i = 0; i < msg.guild.settings.customReactions.length; i++) {
			if (msg.guild.settings.customReactions[i].regex) {
				if (msg.content.toLowerCase().includes(msg.guild.settings.customReactions[i].trigger.toLowerCase())) {
					return msg.sendMessage(msg.guild.settings.customReactions[i].response);
				}
			} else if (msg.content === msg.guild.settings.customReactions[i].trigger) {
				return msg.sendMessage(msg.guild.settings.customReactions[i].response);
			}
		}
		return null;
	}

};
