const { Event } = require('klasa');

module.exports = class extends Event {

	async run(reaction, user) {
		if (reaction.message.partial) await reaction.message.fetch();
		const { message: msg } = reaction;
		this.reactionRoles(msg, reaction, user);
	}

	reactionRoles(msg, reaction, user) {
		if (!msg.guild) return;
		if (user.bot) return;
		for (const rero of msg.guild.settings.reactionRoles) {
			const reactions = new Map(rero.reactions);
			if (msg.id === rero.message && msg.channel.id === rero.channel) {
				if (reactions.has(reaction.emoji.name)) msg.guild.members.get(user.id).roles.remove(reactions.get(reaction.emoji.name));
				if (reactions.has(reaction.emoji.id)) msg.guild.members.get(user.id).roles.remove(reactions.get(reaction.emoji.id));
			}
		}
	}

};
