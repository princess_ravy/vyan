/* eslint-disable complexity */
const { Event } = require('klasa');
const { MessageEmbed } = require('discord.js');
const req = require('centra');
const ksoftBaseURL = 'https://api.ksoft.si/bans/check';
const DSBaseURL = 'https://discord.services/api/ban/';

module.exports = class extends Event {

	async run(member) {
		if (!member || !member.guild) return;
		if (
			member.guild.id === '446658603873730611' &&
			(/\w+?\d{1,6}/u).test(member.user.username) &&
			(Date.now() - member.user.createdTimestamp < 1000 * 60 * 60 * 24 * 7)
		) this.client.emit('spambot', member);
		await this.autoRoles(member);
		await this.autoNick(member);
		await this.mutePersistency(member, member.guild);
		const msg = await this.joinLog(member);
		if (msg) await this.securityCheck(member, msg);
		return;
	}

	async autoRoles(member) {
		if (member.guild.settings.autoRole && !member.user.bot) {
			const autorole = member.guild.roles.get(member.guild.settings.autoRole);
			if (!autorole) return;
			return member.roles.add(autorole).catch(() => null);
		} else if (member.guild.settings.botRole && member.user.bot) {
			const botrole = member.guild.roles.get(member.guild.settings.botRole);
			if (!botrole) return;
			return member.roles.add(botrole).catch(() => null);
		}
		return;
	}

	async autoNick(member) {
		const { nickPrefix, nickSuffix } = member.guild.settings;

		if (!nickPrefix || !nickSuffix) return;

		return member.setNickname(`${nickPrefix || ''} ${member.user.username} ${nickSuffix || ''}`).catch(() => null);
	}

	async joinLog(member) {
		if (!member.guild.settings.log.members) return;
		const embed = new MessageEmbed()
			.setAuthor(`${member.user.bot ? 'Bot' : 'User'} joined: ${member.user.tag}`, member.user.displayAvatarURL({ format: 'png', size: 256 }))
			.setFooter(`member #${member.guild.memberCount}`)
			.setColor(0x4CAF50);
		if (member.guild.settings.security && !member.user.bot) embed.setDescription('Background checks running');

		const msg = await member.guild.channels.get(member.guild.settings.log.members).send({ embed });
		return msg;
	}

	async securityCheck(member, msg) {
		if (!member.guild.settings.security || member.user.bot) return;

		const syncBanned = this.client.settings.syncBannedUsers.includes(member.id);

		const res = await req(ksoftBaseURL)
			.query('user', member.id)
			.header('Authorization', `Bearer ${process.env.API_KSOFT}`)
			.send()
			.then(r => r.json())
			.catch(e => console.log(e));
		const ksoftBanned = res.is_banned;

		const res2 = await req(DSBaseURL)
			.path(member.id)
			.send()
			.then(r => r.json());
		const DSBanned = Boolean(res2.ban);

		const accountAge = Date.now() - member.user.createdAt <= 86400 * 1000;

		const defaultAvatar = !member.user.avatar;


		const embed = new MessageEmbed()
			.setAuthor(`User joined: ${member.user.tag}`, member.user.displayAvatarURL({ format: 'png', size: 256 }))
			.setDescription([
				syncBanned
					? '<:vCross:492961655265951744> Banned from V-Network'
					: null,
				ksoftBanned
					? '<:vCross:492961655265951744> Banned on KSoft.Si Bans'
					: null,
				DSBanned
					? '<:vCross:492961655265951744> Banned on Discord Services'
					: null,
				accountAge
					? '<:vCross:492961655265951744> Account less than 24h old'
					: null,
				defaultAvatar
					? '<:vCross:492961655265951744> Using the default avatar'
					: null,
				'',
				syncBanned || ksoftBanned || DSBanned
					? `<:vCross:492961655265951744> Globally banned user detected and banend | [Click here for more info](https://bans.ksoft.si/check/${member.id}).`
					: defaultAvatar || accountAge
						? `<:vCross:492961655265951744> Potentially unsafe | [Click here for more info](https://bans.ksoft.si/check/${member.id}).`
						: `<:vCheck:492961725201776642> User is safe | [Click here for more info](https://bans.ksoft.si/check/${member.id}).`
			].filter(a => a !== null).join('\n'))
			.setFooter(`Powered By KSoft.Si | member #${member.guild.memberCount}`)
			.setColor(syncBanned || ksoftBanned || DSBanned ? 0xF44336 : 0x4CAF50);
		await msg.edit({ embed });
		if (syncBanned || ksoftBanned || DSBanned) {
			return member
				.ban({ reason: `${this.client.user.tag} | globally banned user detected` })
				.catch(() => null);
		}
	}

	async mutePersistency(member, guild) {
		if (!guild.settings.muteRole) return;
		if (member.settings.muted) return member.roles.add(guild.settings.muteRole);
		return;
	}

};
