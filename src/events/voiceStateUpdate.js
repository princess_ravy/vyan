const { Event } = require('klasa');

module.exports = class extends Event {

	constructor(...args) {
		super(...args, {
			name: 'voiceStateUpdate',
			enabled: true,
			event: 'voiceStateUpdate'
		});
	}

	async run(oldState, newState) {
		if (newState.selfMute !== oldState.selfMute) return this.client.emit(`voiceSelf${newState.selfMute ? 'Unm' : 'M'}ute`, oldState, newState);
		if (newState.selfDeaf !== oldState.selfDeaf) return this.client.emit(`voiceSelf${newState.selfDeaf ? 'Und' : 'D'}eaf`, oldState, newState);

		if (newState.serverMute !== oldState.serverMute) return this.client.emit(`voiceServer${newState.serverMute ? 'Unm' : 'M'}ute`, oldState, newState);
		if (newState.serverDeaf !== oldState.serverDeaf) return this.client.emit(`voiceServer${newState.serverDeaf ? 'Und' : 'D'}eaf`, oldState, newState);

		if (!oldState || !oldState.channel) return this.client.emit('voiceConnect', newState);

		if (!newState.channel) return this.client.emit('voiceDisconnect', oldState);

		if (newState.channel.id !== oldState.channel.id) return this.client.emit('voiceMove', oldState, newState);

		if (oldState.speaking !== newState.speaking) return this.client.emit(`voice${newState.speaking ? 'Start' : 'Stop'}Speaking`, oldState, newState);
	}

};
