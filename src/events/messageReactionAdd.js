const { Event } = require('klasa');
const { MessageEmbed } = require('discord.js');
const stars = ['⭐', '🌟'];

module.exports = class extends Event {

	async run(reaction, user) {
		if (reaction.message.partial) await reaction.message.fetch();
		const { message: msg } = reaction;
		this.reactionRoles(msg, reaction, user);
		if (!msg.guild || !msg.guild.settings.starChannel) return;
		if (!(reaction.count >= 3 && reaction.emoji.name === stars[0])) return;

		const previous = await msg.guild.settings.starred.find(entry => entry.messageID === msg.id && entry.channelID === msg.channel.id);
		if (!previous) return this.newStarred(msg, reaction.count);
		return this.editStarred(msg, previous, reaction.count);
	}

	async newStarred(msg, reactionCount) {
		const starChannel = await msg.guild.channels.get(msg.guild.settings.starChannel);
		if (!starChannel) return;
		const options = {};
		if (msg.attachments.first()) options.files = msg.attachments.map(a => ({ name: a.filename, attachment: a.url }));
		options.embed = this.generateMessage(msg, reactionCount);
		const starMessage = await starChannel.send(options);
		return msg.guild.settings.update('starred', { messageID: msg.id, channelID: msg.channel.id, starMessageID: starMessage.id }, { action: 'add' });
	}

	async editStarred(msg, entry, reactionCount) {
		const starChannel = await msg.guild.channels.get(msg.guild.settings.starChannel);
		if (!starChannel) return;
		const starMessage = await starChannel.messages.fetch(entry.starMessageID);
		if (!starMessage) return;

		const options = {};
		if (msg.attachments.first()) options.files = msg.attachments.map(a => ({ name: a.filename, attachment: a.url }));
		options.embed = this.generateMessage(msg, reactionCount);
		return starMessage.edit(options);
	}

	generateMessage(msg, reactionCount) {
		const embed = new MessageEmbed()
			.setTimestamp(msg.createdAt)
			.setAuthor(msg.author.tag, msg.author.avatarURL({ format: 'png', size: 512 }))
			.setColor(msg.author.displayColor)
			.setFooter(`${reactionCount >= 5 ? stars[1] : stars[0]} ${reactionCount} - in #${msg.channel.name}`);
		if (msg.content) embed.setDescription(msg.content);
		return embed;
	}

	reactionRoles(msg, reaction, user) {
		if (user.bot) return;
		if (!msg.guild) return;
		for (const rero of msg.guild.settings.reactionRoles) {
			const reactions = new Map(rero.reactions);
			if (msg.id === rero.message && msg.channel.id === rero.channel) {
				if (reactions.has(reaction.emoji.name)) msg.guild.members.get(user.id).roles.add(reactions.get(reaction.emoji.name));
				if (reactions.has(reaction.emoji.id)) msg.guild.members.get(user.id).roles.add(reactions.get(reaction.emoji.id));
			}
		}
	}

};
