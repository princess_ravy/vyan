const { Event } = require('klasa');
const { MessageEmbed } = require('discord.js');
const { error } = require('../lib/util/constants').colors;

module.exports = class extends Event {

	async run({ data, message }) {
		const msg = this.client.channels.get(message.channel).messages.get(message.id);
		const filter = m => m.author.id === data.moderator;
		await msg.reactions.removeAll();
		await msg.edit(msg.embeds[0].setColor(error).setFooter(`id: ${data.id} | mod: ${this.client.users.get(data.moderator).tag}`));
		const m1 = await msg.channel.send('Reason for denying this report:');
		const c = await msg.channel.createMessageCollector(filter, { time: 60000 });
		c.on('collect', async (m) => {
			c.stop();
			const embed = new MessageEmbed()
				.setTitle(`Report denied by ${this.client.users.get(data.moderator).tag}`)
				.setDescription(`Denial reason: \`${m.content}\``)
				.addField('user', this.client.users.get(data.user).tag, true)
				.addField('reason', data.reason, true)
				.addField('proof', data.proof, true)
				.setFooter(`id: ${data.id}`)
				.setColor(error);
			await m1.delete();
			await m.delete();
			return this.client.users.get(data.requester).send({ embed });
		});
	}

};
