const { Event } = require('klasa');
const { success } = require('../lib/util/constants').colors;
const req = require('centra');

module.exports = class extends Event {

	async run({ data, message }) {
		const msg = this.client.channels.get(message.channel).messages.get(message.id);
		await msg.reactions.removeAll();
		await msg.edit(msg.embeds[0].setColor(success).setFooter(`id: ${data.id} | mod: ${this.client.users.get(data.moderator).tag}`));
		await this.client.settings.update('syncBannedUsers', { ...data }, { action: 'add' });
		const user = await this.client.users.fetch(data.user);
		await req('https://api.ksoft.si', 'POST')
			.path('/bans/add')
			.body({
				user: user.id,
				mod: data.requester,
				user_name: user.username,
				user_discriminator: user.discriminator,
				reason: data.reason,
				proof: data.proof
			}, 'form')
			.header('Authorization', `Bearer ${process.env.API_KSOFT}`)
			.send()
			.catch(() => null);
		return this.client.syncBan(data.user, data.reason);
	}

};
