const { Event } = require('klasa');
const { MessageEmbed } = require('discord.js');

module.exports = class extends Event {

	async run(oldMessage, newMessage) {
		if (newMessage.partial) await newMessage.fetch();
		if (this.client.ready && oldMessage.content !== newMessage.content) this.client.monitors.run(newMessage);
		if (oldMessage.author.bot || oldMessage.content === newMessage.content) return;
		const { settings: { log: { messages: messageLog } } } = oldMessage.member.guild;
		if (!messageLog) return;
		const channel = oldMessage.guild.channels.get(messageLog);
		const embed = new MessageEmbed()
			.setTitle(`message edited/${oldMessage.author.tag}`)
			.setDescription([
				'**old message:**',
				oldMessage.content || 'no content',
				'',
				'**new message:**',
				newMessage.content || 'no content'
			].join('\n'))
			.setFooter(`in #${oldMessage.channel.name}`)
			.setColor(0xFFC107);
		if (channel) channel.send({ embed });
	}

};
