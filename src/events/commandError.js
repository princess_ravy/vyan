const { Event } = require('klasa');

module.exports = class extends Event {

	run(message, command, params, error) {
		if (typeof error === 'string') return message.responder.error(error);
		const code = this.client.generateSnowflake();
		this.client.settings.update('errors', { code, error: error.stack || error.message || error, command: command.name, params, args: message.args });
		message.responder.error(`**Error:** ${error}\n\n*For support, provide code \`${code}\`*`)
			.catch(() => message.responder.error(`**Error:** [too long]\n\n*For support, provide code \`${code}\`*`));
		// if (error instanceof Error) this.client.emit('wtf', `[COMMAND] ${command.path}\n${error.stack || error}`);
		// if (error.message) message.sendCode('JSON', error.message).catch(err => this.client.emit('wtf', err));
		// else message.responder.error(error).catch(err => this.client.emit('wtf', err));
	}

};
