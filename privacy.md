# Privacy Policy
Last modified: November 13th, 2021

## 1. Why and how do we collect your data?

Vyan mainly provides an automated application for chat and social platforms, (the "Service") and is operated by The Vyan Team (the "Team", "we", "us") for users of the Service (“you”). This Privacy Policy sets forth our policy with respect to information that is collected from users of the Service. Under applicable law, The Vyan Team is the "data controller" of personal data collected throughout your usage of the Service.

## 2. What information do you collect?

When you interact with us through the Service, we may collect information from you, as further described below.
We collect information from you when you voluntarily provide such information, such as when you register for access to our Services. Information we collect may include but are not limited to:
(i) Chat data: We store username, user discriminator, user id, email address, and any messages via the chat feature of a chat or social platform temporarily to support you and others in preventing spam on aforementioned platform. We share anonymized message data with Perspective, a third party spam prevention service, if enabled on the community (default off).
(ii) Guild data For all the servers you add Vyan to, we store guild name, guild id, the guild owner's id, nicknames and roles, as well as the user data of active members as described in (i).
(iii) Message logs If a server administrator enables the message logging feature, Vyan will copy all deleted and edited messages in a text channel on that server which you might not have access to. You can contact us at privacy.vyan@farfrom.earth to find out which channel this data is stored in. This data is not stored persistently on any of our servers.

## 3. Where is your data processed?

The Team is based in Germany. No matter where you are located, you consent to the processing and transferring of your information in and to Germany and other countries. The laws of Germany and other countries governing data collection and use may not be as comprehensive or protective as the laws of the country where you live.
Our servers are located in Germany (Falkenstein).

## 4. What do you do with my data?

We use the information you provide in a manner that is consistent with this Privacy Policy. If you provide information for a certain reason, we may use the information in connection with the reason for which it was provided. For instance, if you contact us by email, we will use the information you provide to answer your question or resolve your problem. Also, if you provide information in order to obtain access to the Services, we will use your information to provide you with access to such services and to monitor your use of such services.
We use your data to:
(i) Monitor your compliance with our Terms of Service.
(ii) Allow you to persist and store data and settings.
(iii) Provide you with technology to combat unsolicited (marketing) messages, i.e. spam.
(iv) Provide you with technology to combat unwanted users and messages, e.g. through our toxicity and profanity filters.
(v) Provide community administrators with aforementioned technology. (vi) Comply with legal requirements.
We do not sell your data.

## 5. How long do you store data?

We try to keep the duration that we store your data as low as possible. At most we store:
(i) Your login credentials to our Service until you terminate your account.
(ii) Your user data gathered from inside a chat or social platform for no longer than 30 days, as this is only cached and never saved to disk, this is usually considerably shorter.
(iii) Your messages gathered from inside a chat or social platform for no longer than 30 days, as this is only cached and never saved to disk, this is usually considerably shorter. This excludes message logs as they're not stored on our servers, ever.
(iv) Perspective API stores your data as defined in their privacy policy.

## 6. What data do you store about me?

If you want to get access to the exact data we store, please contact us via email at privacy.vyan@farfrom.earth with any means of identifying which data belongs to you, e.g. for data related to Discord, this would be a user id. We will then start the process and provide you with a collection of all personal data we collected about you within 30 days, given the identification you provided was sufficient. You have a right to ask us to stop using or limit our use of your personal data in certain circumstances - for example, if we have no lawful basis to keep using your data, or if you think your personal data is inaccurate. In this case, please contact us via email at privacy.vyan@farfrom.earth with any means of identifying which data belongs to you, e.g. for data related to Discord, this would be a user id. We will then start the process and remove or modify all required personal data we collected about you within 30 days.

## Final

We reserve the right to update or modify this Privacy Policy at any time and from time to time without prior notice. Please review this policy periodically, and especially before you provide any information. This Privacy Policy was last updated on the date indicated above. Your continued use of the Service after any changes or revisions to this Privacy Policy shall indicate your agreement with the terms of such revised Privacy Policy.
